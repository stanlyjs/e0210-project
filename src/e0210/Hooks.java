package e0210;

import java.util.HashMap;

public class Hooks {
	
	private static int counter;
	
	private static String[] threadIds =new String[10000]; //TID to String mapping
	private static int[] childsForked =new int[10000]; // TID to no. of children (used to increment ThreadId)	
	private static HashMap<String,HashMap<Integer,String>>[] methodSigCountBlids = new HashMap[10000]; //TID to method signature,methodcount,blids
//	private static String[] blids = new String[10000]; //TID to BLIDS for every TID at every function (may not work for direct recursion)
	
	public static synchronized void initializeCounter(int val) {
	    counter=val ;
	  }
	
	public static synchronized void incrementCounter(int val) {
		    counter+=val ;
		  }
	
	public static synchronized void printCounter()
	{
		System.out.println(counter);
	}
	
	public static synchronized void insertOnBackEdge(int dest,int src)
	{
		counter +=dest;
		System.out.println(counter);
		counter=src;
	}

	public static synchronized void printNewLine()
	{
		System.out.println();
	}
	
	//Initialize at the beginning of main
	public static synchronized void addMainThread(){
		long pid = Thread.currentThread().getId();
		threadIds[(int)pid]="0";
	}
	
	//After tx.start()
	public static synchronized void addChildThread(long cid){
		long pid = Thread.currentThread().getId(); //get parent id
		threadIds[(int)cid]=threadIds[(int)pid]+"."+childsForked[(int)pid]; //cid=pid.threadCount
		childsForked[(int)pid]+=1;
	}
	
	
	
	//Initialize at the beginning of every function
		public static synchronized long analyzeMethodCall(String signature){
			
			long methodCounter = 0;
			long tid = Thread.currentThread().getId();
//			System.out.println("Method" + signature + "called by thread" + tid);
			if(methodSigCountBlids[(int) tid]==null) //Thread has not made call to any function
			{
				methodSigCountBlids[(int) tid]=new HashMap<String,HashMap<Integer,String>>();
				
				//Initialize inner Hashmap
				
				HashMap<Integer,String> temp = new HashMap<Integer,String>();
				temp.put(1, null); //Initialize method count to 1 and Blids to empty string
				
				methodSigCountBlids[(int)tid].put(signature, temp);
			}
			else //Thread has made call to atleast one function
			{
				if(methodSigCountBlids[(int)tid].containsKey(signature)) //If Thread has already made a call to this function before
				{
					if(!signature.contains("run")) //Same thread will not call run twice, that means its a self loop like while
					{
						HashMap<Integer,String> temp = methodSigCountBlids[(int)tid].get(signature);
						//For this hashmap find the highest value of keyset() and put the same value of blids in it
						int max = 0;
						for(int i:temp.keySet())
						{
							if(i>max)
								max=i;
						}
						temp.put(max+1, null); //Confirm if putting empty blids is correct or should put value of max count's blid
						methodSigCountBlids[(int)tid].put(signature, temp); //Increment its count by 1
					}
						
				}
				else
				{
					HashMap<Integer,String> temp = new HashMap<Integer,String>();
					temp.put(1, null); //Initialize method count to 1 and Blids to empty string
					
					methodSigCountBlids[(int)tid].put(signature, temp);
				}
			}
			HashMap<Integer,String> temp = methodSigCountBlids[(int)tid].get(signature);
			//For this hashmap find the highest value of keyset() and put the same value of blids in it
			int max = 0;
			for(int i:temp.keySet())
			{
				if(i>max)
					max=i;
			}
			methodCounter = max;
			return methodCounter;
			
		}
		
		public static synchronized void addCounter(long counter, String signature){
			long tid = Thread.currentThread().getId();
			
			//Find latest method count for this TID and method signature			
			
			HashMap<Integer,String> temp = methodSigCountBlids[(int)tid].get(signature);
			//For this hashmap find the highest value of keyset() and put the same value of blids in it
			int max = 0;
			for(int i:temp.keySet())
			{
				if(i>max)
					max=i;
			}
			
			String blids = methodSigCountBlids[(int) tid].get(signature).get(max);
			if(blids==null) // blid sequence doesnt exist
			{
				blids=Integer.toString((int)counter);
				//Add to existing hashmap
				methodSigCountBlids[(int) tid].get(signature).put(max, blids);
			}
				
			else
			{
				blids=blids+"->"+Integer.toString((int)counter);
				//Add to existing hashmap
				methodSigCountBlids[(int) tid].get(signature).put(max, blids);
			}
				
		}
		
		public static synchronized void addCounterAndPrintTuple(long counter, long methodCounter, String signature){
			long tid = Thread.currentThread().getId();
			
			//Get highest method count so far
			
			HashMap<Integer,String> temp = methodSigCountBlids[(int)tid].get(signature);
			//For this hashmap find the highest value of keyset() and put the same value of blids in it
			int max = 0;
			for(int i:temp.keySet())
			{
				if(i>max)
					max=i;
			}
			
			String blids = methodSigCountBlids[(int) tid].get(signature).get(max);
			if(blids!=null)
				System.out.println(threadIds[(int)tid]+","+signature+","+methodCounter+","+blids+"->"+counter);
			else
				System.out.println(threadIds[(int)tid]+","+signature+","+methodCounter+","+counter);
			
//			blids[(int)tid]=null; //Assumes no recursion (I guess) Check for later errors
			//If you want to fix recursion later in the future, enable 
			//1) Debugging comment in analyzeMethodCall and 2) Enable print (counter) in Analyze.java in those two places.
			//And study test case8 and factorial example given in piazza
		}
	
	
	
	
	
	
	
	
	
	
	
//	//Initialize at the beginning of every function
//	public static synchronized void analyzeMethodCall(String signature){
//		
//		long tid = Thread.currentThread().getId();
////		System.out.println("Method" + signature + "called by thread" + tid);
//		if(methodSigCount[(int) tid]==null) //Thread has not made call to any function
//		{
//			methodSigCount[(int) tid]=new HashMap();
//			methodSigCount[(int)tid].put(signature, 1);
//		}
//		else //Thread has made call to atleast one function
//		{
//			if(methodSigCount[(int)tid].containsKey(signature)) //If Thread has already made a call to this function before
//			{
//				if(!signature.contains("run")) //Same thread will not call run twice, that means its a self loop like while
//					methodSigCount[(int)tid].put(signature, methodSigCount[(int)tid].get(signature)+1); //Increment its count by 1
//			}
//			else
//			{
//				methodSigCount[(int)tid].put(signature,1);
//			}
//		}
//		
//	}
//	
//	public static synchronized void addCounter(long counter, String signature){
//		long tid = Thread.currentThread().getId();
//		//HANDLE FOR UNIQUE METHOD COUNT. CHANGE BLID DATA STRUCTURE TO HASHMAP FOR TEST CASE 8
//		if(blids[(int) tid]==null)
//			blids[(int) tid]=Integer.toString((int)counter);
//		else
//			blids[(int) tid]=blids[(int) tid]+"->"+Integer.toString((int)counter);
//	}
//	
//	public static synchronized void addCounterAndPrintTuple(long counter, String signature){
//		long tid = Thread.currentThread().getId();
//		if(blids[(int)tid]!=null)
//			System.out.println(threadIds[(int)tid]+","+signature+","+methodSigCount[(int)tid].get(signature)+","+blids[(int)tid]+"->"+counter);
//		else
//			System.out.println(threadIds[(int)tid]+","+signature+","+methodSigCount[(int)tid].get(signature)+","+counter);
//		
//		blids[(int)tid]=null; //Assumes no recursion (I guess) Check for later errors
//		//If you want to fix recursion later in the future, enable 
//		//1) Debugging comment in analyzeMethodCall and 2) Enable print (counter) in Analyze.java in those two places.
//		//And study test case8 and factorial example given in piazza
//	}
	
}
