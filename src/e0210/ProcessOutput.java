package e0210;

import java.io.FileNotFoundException;
import java.io.FileReader;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.Iterator;

public class ProcessOutput {

	public static void main(String[] args) throws IOException {

		String project = args[0];
		String testcase = args[1];

		String inPath = "Testcases/" + project + "/output/" + testcase;
		String outPath = "Testcases/" + project + "/processed-output/" + testcase;

		int curr=-1,prev=10000;
		System.out.println("Processing " + testcase + " of " + project);

		// Read the contents of the output file into a string
		String in = new String(Files.readAllBytes(Paths.get(inPath)));

		// Write the contents of the string to the output file
		PrintWriter out = new PrintWriter(outPath);
		
		FileReader BLMap,inFile;
		try {
			
			BLMap = new FileReader("BLMapping");
			inFile = new FileReader(inPath);
			Scanner BLscanner = new Scanner(BLMap);
			Scanner inScanner = new Scanner(inFile);
		    String temp;
		    Boolean isFirst=true;
		    LinkedList<Integer> methodHeads = new LinkedList<Integer>();
		    
		    //Get information about methods from BLMapping
		    Scanner tempBLScanner = new Scanner(new FileReader("BLMapping")); // Dunno why copying existing scanner to another resulted in not displaying the first line
		    String[] tempBL = tempBLScanner.nextLine().split(";");//First Line
		    int first = Integer.parseInt(tempBL[0].replace("\n", ""));
		    methodHeads.add(first);
		    
		    while(tempBLScanner.hasNextLine())
		    {
		    	if(tempBLScanner.nextLine().isEmpty() && tempBLScanner.hasNextLine())
		    	{
		    		tempBL = tempBLScanner.nextLine().split(";");//First Line
				    first = Integer.parseInt(tempBL[0].replace("\n", ""));
				    methodHeads.add(first);
		    	}
		    }
		    
		    System.out.println(methodHeads); //Contains start pathId of every function
		   Boolean nlPrintedBefore=true;
		    
		    while(inScanner.hasNextLine()){
		    	String inScan = inScanner.nextLine();
		    	System.out.println("INSCAN"+inScan);
		    	
		    	if(inScan.equalsIgnoreCase("") && nlPrintedBefore==false)
		    		if(inScanner.hasNextLine()) //Print new line in Soot output.
		    		{out.println();
		    		continue;}
		    		else break;
		    	if(inScan.equalsIgnoreCase("") && nlPrintedBefore == true)
		    	{
		    		nlPrintedBefore=false;	
		    		continue;
		    	}
		        int a = Integer.parseInt(inScan.replace("\n", ""));		        
		        
		        //Logic for println after every method
		        Integer list2[] = new Integer[methodHeads.size()];
		    	list2 = methodHeads.toArray(list2);
		    	
		    	for(int x=0;x<list2.length;x++)
		    	{
		    		if(a>=list2[x])
		    			curr=x;		    			
		    	}
		    	if(curr!=prev && !isFirst)// Except for first element, print newline before all new method calls
		    	{
		    		out.println();
		    		nlPrintedBefore=true;
		    	}
		    	isFirst=false;
		        prev=curr;
		        
		        
		    	while(BLscanner.hasNextLine())
		    	{
		    		String next = BLscanner.nextLine();
		    		System.out.println("NEXT"+next);
		    		if(!(next.isEmpty()))
		    		{
			    		String[] tokens = next.split(";");
			    		int b = Integer.parseInt(tokens[0].replace("\n", ""));
				    	
				        if(a==b)
				        {
				        String[] path = tokens[1].split(","); // Don't worry about blank commas. This removes it
				        
				        for (int z=0;z<path.length;z++)
				        {
				        	if(z==path.length-1 && !inScanner.hasNextInt()) //Last element in path AND SecondLast Line from Input File
				        	{
				        		
				        		out.print(path[z]);
				        		System.out.println("HI"+path[z]);
				        		
				        	}
				        	else
				        	{
				        		out.println(path[z]);
				        	}
							
								
				        }
				        }
				        
			    	}
		    	}

		    
			    	BLMap = new FileReader("BLMapping"); //REINITIALZE Inside loop
			    	BLscanner = new Scanner(BLMap);
		    	
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.close();

		return;
	}

}