package e0210;

import java.util.Iterator;

/*
 * @author Stanly John Samuel	-		stanly.samuel@csa.iisc.ernet.in
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.LongType;
import soot.Modifier;
import soot.RefType;
import soot.Scene;
import soot.SootField;
import soot.Unit;
import soot.jimple.*;
import soot.util.Chain;

public class Analysis1 extends BodyTransformer {

    private boolean addedGlobalCounter = false;
	    		
	protected void internalTransform(Body b, String phaseName, Map<String, String> options) {
		
		SootField globalCounter = null;
		boolean isFirstNonIdentityStmt = true;
		
		System.out.println("Original Method body is"+ b.toString());
		
        Chain<Unit> units = b.getUnits();

            if (!Scene.v().getMainClass().declaresMethod("void main(java.lang.String[])"))
            	throw new RuntimeException("main() not found");

            if (addedGlobalCounter)
            {
            	globalCounter = Scene.v().getMainClass().getFieldByName("globalCount");
            }
                
            else
            {
                globalCounter = new SootField("globalCount",LongType.v(), Modifier.STATIC);                    
                Scene.v().getMainClass().addField(globalCounter);
                addedGlobalCounter = true;
            }
                  
            Local tmp1 = Jimple.v().newLocal("tmp1", LongType.v());
            b.getLocals().add(tmp1);
            Local refLocal = Jimple.v().newLocal("refLocal", RefType.v("java.io.PrintStream"));
            b.getLocals().add(refLocal);
            Local counter = Jimple.v().newLocal("tmpCounter", LongType.v()); 
            b.getLocals().add(counter);
            
            Iterator<Unit> stmtIt = units.snapshotIterator();
            
            while(stmtIt.hasNext())
            {
                Stmt s = (Stmt) stmtIt.next();
                
                //Insert counter = 0 after all Identity statements in a method.
                if (!(s instanceof IdentityStmt) && isFirstNonIdentityStmt)
                {
                	units.insertBefore(Jimple.v().newAssignStmt(counter, LongConstant.v(0) ), s);
                	isFirstNonIdentityStmt = false;
                }                

                if(s instanceof IfStmt)
                {    
                    //counter = counter + 1;
                    units.insertBefore(Jimple.v().newAssignStmt(counter, Jimple.v().newAddExpr(counter, LongConstant.v(1))), s);
                
                }
                else if (s instanceof InvokeStmt)
                {
                    InvokeExpr iexpr = (InvokeExpr) ((InvokeStmt)s).getInvokeExpr();
                    if (iexpr instanceof StaticInvokeExpr)
                    {                        
                        if (((StaticInvokeExpr)iexpr).getMethod().getSignature().equals("<java.lang.System: void exit(int)>"))
                        {                        	
                        	//Print counter value
                        	
	        	    			// refLocal = java.lang.System.out;
	        	                units.insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
	        	                	                
	        	                // refLocal.println(counter);                        
	        	                units.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal,Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(long)").makeRef(),counter)), s);
        		                
        	                //Increment globalCounter with counter for every return
        	                
	        	                // tmp1 = globalCounter
	        	                units.insertBefore(Jimple.v().newAssignStmt(tmp1, Jimple.v().newStaticFieldRef(globalCounter.makeRef())), s);
	        	                	                
	        	                // tmp1 = tmp1 + counter
	                            units.insertBefore(Jimple.v().newAssignStmt(tmp1, Jimple.v().newAddExpr(tmp1, counter)), s);
	        	                
	        	                // globalCounter = tmp1
	        	                units.insertBefore(Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(globalCounter.makeRef()), tmp1), s);
        	                
        	                //In case of System.exit, print current globalCounter value
        	               
	    	                	// refLocal = java.lang.System.out;
	    	                	units.insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
	    	                	
	    	                	// tmp1 = globalCounter;
	    	                	units.insertBefore(Jimple.v().newAssignStmt(tmp1, Jimple.v().newStaticFieldRef(globalCounter.makeRef())), s);	                	
	    	                	
	    	                	// refLocal.print(tmp1);
	    	                	units.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal, Scene.v().getSootClass("java.io.PrintStream").getMethod("void print(long)").makeRef() ,tmp1)), s);
    	                
                        }
                    }
                }
                else if (s instanceof ReturnStmt || s instanceof ReturnVoidStmt)
                {
                	//Print counter
                	
		    			// refLocal = java.lang.System.out;
		                units.insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
		                	                
		                // refLocal.println(counter);                        
		                units.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal,Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(long)").makeRef(),counter)), s);
		                
	                //Increment globalCounter with counter for every return
	                
		                // tmp1 = globalCounter
		                units.insertBefore(Jimple.v().newAssignStmt(tmp1, Jimple.v().newStaticFieldRef(globalCounter.makeRef())), s);
		                	                
		                // tmp1 = tmp1 + counter
	                    units.insertBefore(Jimple.v().newAssignStmt(tmp1, Jimple.v().newAddExpr(tmp1, counter)), s);
		                
		                // globalCounter = tmp1
		                units.insertBefore(Jimple.v().newAssignStmt(Jimple.v().newStaticFieldRef(globalCounter.makeRef()), tmp1), s);
	               
	                //If return of main, print globalCounter without new line
	                
	                if (b.getMethod().getSubSignature().equals("void main(java.lang.String[])"))
	                {
	                	// refLocal = java.lang.System.out;
	                	units.insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
	                	
	                	// tmp1 = globalCounter;
	                	units.insertBefore(Jimple.v().newAssignStmt(tmp1, Jimple.v().newStaticFieldRef(globalCounter.makeRef())), s);	                	
	                	
	                	// refLocal.print(tmp1);
	                	units.insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal, Scene.v().getSootClass("java.io.PrintStream").getMethod("void print(long)").makeRef() ,tmp1)), s);
	                }           
            }
        }
        System.out.println("New Method Body is"+ b.toString());
    }

}




