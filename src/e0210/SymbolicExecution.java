package e0210;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.jimple.*;
import soot.jimple.internal.*;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.util.Chain;

import com.microsoft.z3.*;
import com.microsoft.z3.Expr;
public class SymbolicExecution extends SceneTransformer {

	String project;
	String testcase;

	// Input args that were given to the testcase executable. 
	// These may be need during symbolic execution
	String argsFile;
	String returnValue = ""; //For parameters
	//For clinit() final orderVariable
	String clinitLastOV="";
	HashMap<String,Integer> counter= new HashMap<String,Integer>(); // HashMap of counters responsible for order variable counters of each thread
	
	HashMap<String,HashMap<Integer,String>> intraThreadTrace = new HashMap<String,HashMap<Integer,String>>(); //(TID,MethodSignature,MethodCount) to (Line Number, IntraThreadTrace)
	HashMap<String,String> pathConstraint = new HashMap<String,String>(); //Path Constraints for each Thread (TID to Path Constraint)
	
	//HashMap to check count of a threads method call
	HashMap <String,Integer> threadMethodCount = new HashMap<String,Integer>();
	
	//Hashmap to record read/write of static variables and write of local variables
	
	HashMap<String,Integer> localWriteCount = new HashMap<String,Integer>();
	HashMap<String,Integer> localWriteCountWasNull = new HashMap<String,Integer>();
	HashMap<String,Integer> staticReadCount = new HashMap<String,Integer>();
	HashMap<String,Integer> staticWriteCount = new HashMap<String,Integer>();

	//For Fork/Join
	HashMap<String,Integer> childsForked = new HashMap<String,Integer>();
	HashMap<String, String> baseToTid = new HashMap<String,String>();
	
	//For Locking Constraints, maps lock and its correspnding order variables <lock,<(O1,O2),...>>
	HashMap<String,HashMap<String,String>> lockOvSet = new HashMap<String,HashMap<String,String>>();
	
	//For Read Write Constraints, maps Shared Var to map of (SymVal,OrderVariable) pairs
	HashMap<String,HashMap<String,String>> sharedVarRead = new HashMap<String,HashMap<String,String>>();
	HashMap<String,HashMap<String,String>> sharedVarWrite = new HashMap<String,HashMap<String,String>>();
	
	//Initialize z3
	HashMap<String, String> z3map = new HashMap<String, String>();
	Context ctx = new Context(z3map);
	Solver solver = ctx.mkSolver();
	
	//Stores final orderVariables
	ArrayList<Expr> orderVariables = new ArrayList<Expr>();
	//Stores final orderVariables assignment
	ArrayList<Integer> orderVariablesAssignment= new ArrayList<Integer>();
	//Checks if orderVariables finally printed
	ArrayList<Integer> orderVariablesPrinted = new ArrayList<Integer>(); 
 	
	public SymbolicExecution(String project, String testcase , String argsFile){
		this.project=project;
		this.testcase=testcase;
		this.argsFile=argsFile;
	}
	
	@Override
	protected void internalTransform(String phaseName, Map<String, String> options) {

		System.out.println("--------------------------------------Symbolic Internal Transform Begins--------------------------------------");
		
        /* 
        SceneTransformer vs BodyTransformer
        ===================================
        BodyTransformer is applied on all methods separately. It uses multiple 
        worker threads to process the methods in parallel.
        SceneTransformer is applied on the whole program in one go. 
        SceneTransformer uses only one worker thread.
        
        It is better to use SceneTransformer for this part of the project 
        because:
        1. During symbolic execution you may need to 'jump' from one method
        to another when you encounter a call instruction. This is easily 
        done in SceneTransformer (see below)
        2. There is only one worker thread in SceneTransformer which saves 
        you from having to synchronize accesses to any global data structures
        that you'll use, (eg data structures to store constraints, trace, etc)
        
        How to get method body?
        =======================
        Use Scene.v().getApplicationClasses() to get all classes and
        SootClass::getMethods() to get all methods in a particular class. You
        can search these lists to find the body of any method.
        
        */
		 			
		//Get main and run SootMethod to pass it initially
		Chain<SootClass> chain = Scene.v().getApplicationClasses(); //Gets all classes
		Iterator<SootClass> chainIt = chain.iterator();
		SootMethod main=null,run = null, clinit=null;
		
		//To avoid unnecessary main calls
		HashMap<String,Boolean> traversed = new HashMap<String,Boolean>();
		
		//Initialize Z3
        z3map.put("model", "true");
//        z3map.put("unsat_core", "true");
		
		
		while(chainIt.hasNext())
		{
			SootClass class1 = chainIt.next();
			
			//Debugging info
			System.out.println();
			System.out.println("CLASS"+class1);
			System.out.println("MethodsInClass"+class1.getMethods());
			System.out.println();
			//Debugging info
			
			if(class1.getName().contentEquals(testcase+".Main"))
			{
				main = class1.getMethod("void main(java.lang.String[])"); //main = <Test1.Main: void main()...>
				clinit = class1.getMethod("void <clinit>()");
			}	
			
			if(class1.getName().contentEquals(testcase+".Main"+"$MyThread"))
			{
				run = class1.getMethod("void run()");
			}
		}
		
		//Take as input from testcase-tuples the first tuple, if id is 0 get main method else get run method and perform analysis. Simple!
		
		threadWalk("0",clinit,null,null);
		String tupleInPath = "Testcases/" + project + "/processed-output/" + testcase + ".tuples";
		FileReader tupleIn; 

			try {
				tupleIn = new FileReader(tupleInPath);
				Scanner tupleScanner = new Scanner(tupleIn);
				while(tupleScanner.hasNextLine())
				{
					String tuple = tupleScanner.nextLine();
					String[] splitTuple = tuple.split(",");
					//Will run many times when it encounters many "0" or "0.x.x..." However, it will return the same output everytime. Inefficient but ignore for now.
					//Eventually it serves the purpose of making sure every  thread begins with its respective function i.e. main or run
					//Solved by adding traversed hashmap
					if(splitTuple[0].contentEquals("0") && traversed.get(splitTuple[0])==null) // Main Thread
					{
						threadWalk(splitTuple[0],main,clinitLastOV,null);
						System.out.println("MAIN "+tuple);
						traversed.put(splitTuple[0], true);
						
					}
					else if(!(splitTuple[0].contentEquals("0")) && traversed.get(splitTuple[0])==null)//Child threads
					{
						threadWalk(splitTuple[0],run,null,null);
						System.out.println("CHILD "+tuple);
						traversed.put(splitTuple[0], true);						
					}
					System.out.println("TRAVERSED"+traversed);
				}
				tupleScanner.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
//		System.out.println("ITT"+intraThreadTrace);
			printIntraThreadTrace();
			
			//Generate Inter Thread Trace
			generatePartialOrderConstraints();
			generateLockingConstraints();
			generateReadWriteConstraints();
		
			//End of z3 and print conditions
			
			Status ans = solver.check();
			System.out.println(ans);
			Model model=null;
			if(Status.SATISFIABLE==ans)
			{
				model = solver.getModel();
				
				for(int i=0;i<orderVariables.size();i++)
					{
						String ov = orderVariables.get(i).toString();
						int value=Integer.parseInt(model.eval(orderVariables.get(i), true).toString());
						//Add to new ArrayList to print final global trace
						orderVariablesAssignment.add(i, value); // i starts from 0 always hence no NPE
						orderVariablesPrinted.add(i, 0); //None printed so far. Initialize to false
						System.out.println("OVA: "+ov+" :: "+Integer.toString(value));
					}
			}
			else
			{
				System.out.println("----------------------------UNSAT-------------------------");
			}
			

//			System.out.println("UNSAT CORE"+solver.getUnsatCore().);
			System.out.println("OVAvalue: "+orderVariables);
			BoolExpr[] bool = solver.getAssertions();
			//Print all constraints
			for(int i=0;i<bool.length;i++)
				System.out.println(bool[i]);
			System.out.println("TOTAL CONSTRAINTS "+bool.length);
			//Print Global Trace
			try {
				printGlobalTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        /* 
        Perform the following for each thread T:
        1. Start with the first method of thread T. If T is the main thread,
        the first method is main(), else it is the run() method of the thread's
        class
        2. Using (only) the tuples for thread T, walk through the code to 
        reproduce the path followed by T. If thread T performs method calls
        then, during this walk you'll need to jump from one method's body to 
        another to imitate the flow of control (as discussed during the Project session)
        3. While walking through the code, collect the intra-thread trace for 
        T and the path constraints.
        Don't forget that you need to renumber each dynamic instance of a static
        variable (i.e. SSA)
        */
	    return;
	}

	
	protected void threadWalk(String threadId, SootMethod method, String preOv, List<Expr> localParameters)
	{
//		System.out.println("RETURN TYPE"+method.getReturnType());
		//Initialize count of method, else Increment
		if(threadMethodCount.get(threadId+method.getSignature().toString())==null)
		{
			threadMethodCount.put(threadId+method.getSignature().toString(), 1);			
		}
		else
		{
			threadMethodCount.put(threadId+method.getSignature().toString(), threadMethodCount.get(threadId+method.getSignature().toString())+1);
		}
		
		Body b = method.getActiveBody();
		ExceptionalBlockGraph graph = new ExceptionalBlockGraph(b);
		//Get the tuple for the given tid, method signature and method count
		String tuple=getTuple(threadId,method.getSignature(),threadMethodCount.get(threadId+method.getSignature().toString()));
		//To get block sequence, Split tuple by , then split fourth value (block sequences) by -> to get array of block sequences for a tuple
		String[] splitTuple = tuple.split(",");
		String[] blocks =splitTuple[3].split("->");
//		if (method.getSignature().contains("void run()") || (method.getSignature().contains("void <clinit>()")&&method.getSignature().contains(testcase+".Main")))
//			counter = 0; //Counter may be a risky value to index on in case of a recursion
		String prevOrderVariable=preOv;
		String orderVariable = preOv; //Logic: Till the first orderVariable comes (while traversing s), prevOrderVariable will be assigned its own value, else it will be null

		System.out.println("--------------------------------------Thread Walk for "+threadId+splitTuple[1]+splitTuple[2]+" begins--------------------------------------");
		System.out.println("TMC"+threadMethodCount);
		System.out.println("GRAPH\n"+ graph);
		
		//Link clinit to main (i.e the respective order variables) (O_DUMMY will only exist for main. for other functions we will pass prevordervariable to threadWalk
		if(method.getSignature().contains("void main(java.lang.String[]"))
		{
			System.out.println("LINK CLINIT TO MAIN");
			HashMap<Integer, String> x =intraThreadTrace.get("0<"+testcase+".Main: void main(java.lang.String[])>1");
			Set<Integer> set = x.keySet();
			//Get highest counter so far
			int max = 0;
			for(Integer i : set)
			{
				if(i>0)
					max=i;
			}
			String preOV = "O_0_"+max;
			prevOrderVariable=preOV;
			orderVariable=preOV;
//			max=max+1;
//			String OV = "O_0_"+max;
//			System.out.println(preOV);
//			System.out.println(OV);
//			ArithExpr o3 = ctx.mkIntConst(preOV);
//			ArithExpr o4 = ctx.mkIntConst(OV);
//			BoolExpr pConst = ctx.mkLt(o3,o4);
//			solver.add(pConst);
//			System.out.println("PREORDERVARIABLE"+prevOrderVariable);
		}
		
		//Traverse the units in the block IDs in the exceptionalBlockGraph
		for (int i =0; i< blocks.length; i++)
		{
			System.out.println();
			System.out.println("BLock: "+blocks[i]);
			Block block = graph.getBlocks().get(Integer.parseInt(blocks[i]));
			Iterator<Unit> blockIterator = block.iterator();
			Stmt prev = null;
			while(blockIterator.hasNext())
			{
				Stmt s = (Stmt) blockIterator.next();
				System.out.println("PREV"+prevOrderVariable);
				System.out.println("STATEMENT"+s);
				
				//Index for indexing into intraThreadTrace data structure
				String index = threadId+splitTuple[1]+splitTuple[2]; // Unique Indexing (TID,MethodSig, MethodCount)
				//Explicit index for clinit() or for any function called by main
				if(((method.getSignature().contains("void <clinit>()")&&method.getSignature().contains(testcase+".Main")))|| threadId.contentEquals("0"))
					index = "0<"+testcase+".Main: void main(java.lang.String[])>1";
				else //assuming threadId!=0
					index = threadId+"<"+testcase+".Main$MyThread: void run()>1";
				if(counter.get(threadId)==null)
					counter.put(threadId, 0); // Initialize 0 for each thread.
				
				
//				if(!method.getSignature().contains("void main(java.lang.String[])"))
				if(prevOrderVariable==null) //or if (counter ==0). Basically start of thread
				{	System.out.println("YES");
					//Only run and clinit(for main thread) can have begin block and HENCE FIRST ORDER VARIABLE- FINAL
					//Other function calls of same thread will take dummy value and hence it's start must be linked with it's callers last
					// Plus it must be somehow inserted into the intraThread trace of that method call
					if (method.getSignature().contains("void run()") || (method.getSignature().contains("void <clinit>()")&&method.getSignature().contains(testcase+".Main")))
					{
						HashMap<Integer, String> temp = intraThreadTrace.get(index);
						if(temp == null)
							temp = new HashMap<Integer, String>();
						temp.put(counter.get(threadId), "<"+threadId+","+"Begin"+">");
						intraThreadTrace.put(index, temp);
						//PO Constraints
						orderVariable = "O_"+threadId+"_"+counter.get(threadId);
						orderVariables.add(ctx.mkIntConst(orderVariable)); // Add to main List for evaluation
						counter.put(threadId, counter.get(threadId)+1);
					}
					else 
					{
						orderVariable = "O_DUMMY";
						//Added only so that NPE is avoided. Don't add to main list for eval. However, since it is
						//  made INTCONST a value will be assigned. Hence ignore it during global trace
//						orderVariables.add(ctx.mkIntConst(orderVariable)); // Add to main List for evaluation

					}
					
				}
				
				//Handling parameters in function calls
				if (s instanceof IdentityStmt && localParameters != null && localParameters.size()>0) {
					String leftOp = ((IdentityStmt) s).getLeftOp().toString();
					
					if(localWriteCount.get(leftOp)==null)
					{
						localWriteCount.put(leftOp, 1);
					}
					else
					{
						localWriteCount.put(leftOp, localWriteCount.get(leftOp)+1);
					}

					ArithExpr left = ctx.mkIntConst(leftOp+"_"+localWriteCount.get(leftOp));
					
//					Assuming only one parameter and hence only one identity statement
					Iterator<Expr> it = localParameters.iterator();
//					while(it.hasNext())
//					{
						Expr right =it.next();
//					Expr right = localParameters.get(i);
					System.out.println("Added "+left+" = "+right);
					solver.add(ctx.mkEq(left, right));
				}
				
				if(s instanceof IfStmt)
				{
					ArithExpr left=null, right=null;
					Value leftOpVal = ((IfStmt) s).getCondition().getUseBoxes().get(0).getValue();
					Value rightOpVal = ((IfStmt) s).getCondition().getUseBoxes().get(1).getValue();
					
					String leftOp = ((IfStmt) s).getCondition().getUseBoxes().get(0).getValue().toString();
					String rightOp = ((IfStmt) s).getCondition().getUseBoxes().get(1).getValue().toString();
					
					System.out.println("LSJHDHDJDN"+localWriteCount);
					if(leftOpVal instanceof Constant)
					{
						System.out.println(leftOp+"is a constant");
						left = ctx.mkInt(Integer.parseInt(leftOp));
					}
						
					else if (leftOpVal instanceof Local)
					{
						System.out.println(leftOp+"_"+localWriteCount.get(leftOp)+" is a JimpleLocal");
						left = ctx.mkIntConst(leftOp+"_"+localWriteCount.get(leftOp));
					}
					
					if(rightOpVal instanceof Constant)
					{
						System.out.println(rightOp+"is a constant");
						right = ctx.mkInt(Integer.parseInt(rightOp));
					}
						
					else if (rightOpVal instanceof Local)
					{
						System.out.println(rightOp+"_"+localWriteCount.get(rightOp)+" is a JimpleLocal");
						right = ctx.mkIntConst(rightOp+"_"+localWriteCount.get(rightOp));
					}
					
					System.out.println("CONDITION"+((IfStmt) s));
					System.out.println("Left"+leftOp);
					System.out.println("Right"+rightOp);
					System.out.println("Left Successor"+block.getSuccs().get(0).getIndexInMethod());
					System.out.println("Right Successor"+block.getSuccs().get(1).getIndexInMethod());
					
					String condition = ((IfStmt) s).getCondition().toString();
					System.out.println("CONDVAL"+((((IfStmt) s).getCondition())).getClass());
					int leftChild= block.getSuccs().get(0).getIndexInMethod();
					int rightChild= block.getSuccs().get(1).getIndexInMethod();
					if((i+1)<blocks.length) // To avoid AIOBE ;  Ocuured in test case 14 during replay (not record)
					{
						System.out.println("Successor Taken"+blocks[i+1]);
						int successorTaken=Integer.parseInt(blocks[i+1]);
						//In Jimple if condition is true, right child is taken, hence negate for left child
						if(successorTaken==leftChild) //Negate
						{
//							if(pathConstraint.get(threadId)==null)
//								pathConstraint.put(threadId, "true");
//							pathConstraint.put(threadId, pathConstraint.get(threadId)+"^"+"NOT"+condition);
							
							if(((IfStmt)s).getCondition() instanceof JNeExpr)
							{
								BoolExpr pConst = ctx.mkEq(left, right);
								solver.add(pConst);
							}
							if(((IfStmt)s).getCondition() instanceof JEqExpr)
							{
								BoolExpr pConst = ctx.mkNot(ctx.mkEq(left, right));
								solver.add(pConst);
							}
							if(((IfStmt)s).getCondition() instanceof JLtExpr)
							{
								BoolExpr pConst = ctx.mkNot(ctx.mkLt(left, right));
								solver.add(pConst);
							}						
							if(((IfStmt)s).getCondition() instanceof JLeExpr)
							{
								BoolExpr pConst = ctx.mkNot(ctx.mkLe(left, right));
								solver.add(pConst);
							}						
							if(((IfStmt)s).getCondition() instanceof JGtExpr)
							{
								BoolExpr pConst = ctx.mkNot(ctx.mkGt(left, right));
								solver.add(pConst);
							}						
							if(((IfStmt)s).getCondition() instanceof JGeExpr)
							{
								BoolExpr pConst = ctx.mkNot(ctx.mkGe(left, right));
								solver.add(pConst);
							}			
							
						}
						else if (successorTaken==rightChild) //Don't negate
						{
//							if(pathConstraint.get(threadId)==null)
//								pathConstraint.put(threadId, "true");
//							pathConstraint.put(threadId, pathConstraint.get(threadId)+"^"+condition);
//							
							if(((IfStmt)s).getCondition() instanceof JNeExpr)
							{
								BoolExpr pConst = ctx.mkNot(ctx.mkEq(left, right));
								solver.add(pConst);
							}
							if(((IfStmt)s).getCondition() instanceof JEqExpr)
							{
								BoolExpr pConst = ctx.mkEq(left, right);
								solver.add(pConst);
							}
							if(((IfStmt)s).getCondition() instanceof JLtExpr)
							{
								BoolExpr pConst = ctx.mkLt(left, right);
								solver.add(pConst);
							}						
							if(((IfStmt)s).getCondition() instanceof JLeExpr)
							{
								BoolExpr pConst = ctx.mkLe(left, right);
								solver.add(pConst);
							}						
							if(((IfStmt)s).getCondition() instanceof JGtExpr)
							{
								BoolExpr pConst = ctx.mkGt(left, right);
								solver.add(pConst);
							}						
							if(((IfStmt)s).getCondition() instanceof JGeExpr)
							{
								BoolExpr pConst = ctx.mkGe(left, right);
								solver.add(pConst);
							}						
						}
					}
					
//					System.out.println("PC"+pathConstraint);
				}
				
				//FOR ALL ASSIGNMENT STATEMENTS 
				if(s instanceof AssignStmt)
				{
					if (!((
							(AssignStmt) s).getLeftOp().getType().toString().equals("java.lang.Integer")
							|| ((AssignStmt) s).getLeftOp().getType().toString().equals("java.lang.Boolean")
							|| ((AssignStmt) s).getLeftOp().getType().toString().equals("int")
							|| ((AssignStmt) s).getLeftOp().getType().toString().equals("boolean")))
					{
//						//If instance of local variable, assign value to jimple local and continue, dont move ahead, else there will be error while adding constraints
//						if(((AssignStmt) s).getLeftOp() instanceof Local)
//						{
//							String jimpleLocal = ((AssignStmt) s).getLeftOp().toString();
//							if(localWriteCount.get(jimpleLocal)==null)
//							{
//								localWriteCount.put(jimpleLocal, 1); //Increment later if not initialized to 1
//							}
//							else
//							localWriteCount.put(jimpleLocal, localWriteCount.get(jimpleLocal)+1);
//						}
						prevOrderVariable = orderVariable;
						prev=s;
						continue;
					}
					System.out.println("ASSIGNMENT");
					System.out.println("LEFTOP"+((AssignStmt) s).getLeftOp());
//					System.out.println("LEFTOPTYPE"+((AssignStmt) s).getLeftOp().getClass());
					System.out.println("RIGHTOP"+((AssignStmt) s).getRightOp());
//					System.out.println("RIGHTOPTYPE"+((AssignStmt) s).getRightOp().getClass());
					
					//All JimpleLocal writes are assigned a number
					if(((AssignStmt) s).getLeftOp() instanceof Local)
					{
						//Evaluate right expression first
						Expr rightExpr = null;
						
						if(((AssignStmt) s).getRightOp() instanceof BinopExpr)
						{
							ArithExpr left=null, right = null;
							IntExpr ileft = null, iright = null; // For Mod
							BitVecExpr bleft=null, bright = null; //For bit vector ops
							
							BinopExpr bexpr = (BinopExpr)((AssignStmt) s).getRightOp();
							Value leftOpVal = bexpr.getOp1();
							Value rightOpVal = bexpr.getOp2();
							String leftOp = bexpr.getOp1().toString();
							String rightOp = bexpr.getOp2().toString();
							System.out.println("LSJHDHDJDN"+localWriteCount);
							if(leftOpVal instanceof Constant)
							{
								System.out.println(leftOp+"is a constant");
								left = ctx.mkInt(Integer.parseInt(leftOp));
								if(bexpr instanceof RemExpr)
								{
									ileft = ctx.mkInt(Integer.parseInt(leftOp));
								}
								if(bexpr instanceof XorExpr)
								{									
									bleft=ctx.mkInt2BV(64,ctx.mkInt(Integer.parseInt(leftOp)));
								}
							}
								
							else
							{
								int wrCount = localWriteCount.get(leftOp);
									
								System.out.println(leftOp+"_"+wrCount+" is a JimpleLocal");
								left = ctx.mkIntConst(leftOp+"_"+wrCount);
								if(bexpr instanceof RemExpr)
								{
									ileft = ctx.mkIntConst(leftOp+"_"+wrCount);
								}
								if(bexpr instanceof XorExpr)
								{
									bleft=ctx.mkInt2BV(64, ctx.mkIntConst(leftOp+"_"+wrCount));
								}
							}
							
							if(rightOpVal instanceof Constant)
							{
								System.out.println(rightOp+"is a constant");
								right = ctx.mkInt(Integer.parseInt(rightOp));
								if(bexpr instanceof RemExpr)
								{
									iright = ctx.mkInt(Integer.parseInt(rightOp));
								}
								if(bexpr instanceof XorExpr)
								{
									bright=ctx.mkInt2BV(64,ctx.mkInt(Integer.parseInt(rightOp)));
								}
							}
								
							else
							{
								
								int wrCount = localWriteCount.get(rightOp);
								System.out.println(rightOp+"_"+wrCount+" is a JimpleLocal");
								right = ctx.mkIntConst(rightOp+"_"+wrCount);
								if(bexpr instanceof RemExpr)
								{
									iright = ctx.mkIntConst(rightOp+"_"+wrCount);
								}
								if(bexpr instanceof XorExpr)
								{
									bright=ctx.mkInt2BV(64, ctx.mkIntConst(rightOp+"_"+wrCount));
								}
							}
							
							if(bexpr instanceof XorExpr)
							{
								rightExpr = ctx.mkBV2Int(ctx.mkBVXOR(bleft,bright), true);
							}
								
							if(bexpr instanceof AddExpr)
							{
								rightExpr = ctx.mkAdd(left,right);
							}
							
							if(bexpr instanceof SubExpr)
							{
								rightExpr = ctx.mkSub(left,right);
							}
							
							if(bexpr instanceof MulExpr)
							{
								rightExpr = ctx.mkMul(left,right);
							}
							
							if(bexpr instanceof DivExpr)
							{
								rightExpr = ctx.mkDiv(left,right);
							}
							
							if(bexpr instanceof RemExpr)
							{
								rightExpr = ctx.mkMod(ileft,iright);
							}
						}
						
						//Check rightOp of a Jimple Local whether Virtual Invoke or Static Invoke
						if(((AssignStmt) s).getRightOp() instanceof InvokeExpr)
						{
							if(((AssignStmt) s).getRightOp() instanceof VirtualInvokeExpr)
							{
								VirtualInvokeExpr vexpr = (VirtualInvokeExpr)(((AssignStmt) s).getRightOp());
								if(vexpr.getMethod().getName().contains("Value")) //intValue booleanValue etc
									{
									//Assuming type is java.lang.Integer or Boolean, no Double
									//Local assignment Constraints(needed for Path Constraints)
									//Create jimpleLocalSymVal on the fly using its latest write count (assuming it is a JimpleLocal)
									String base = vexpr.getBase().toString();
									rightExpr = ctx.mkIntConst(base+"_"+localWriteCount.get(base));
									System.out.println("VE"+vexpr.getBase());
									}
							}
							//For example : $r1 = $r2 or $r1 = 2
							else if(((AssignStmt) s).getRightOp() instanceof StaticInvokeExpr)
							{
								StaticInvokeExpr stexpr = (StaticInvokeExpr)(((AssignStmt) s).getRightOp());
								if(stexpr.getMethod().getName().contentEquals("valueOf"))
								{
									Value argVal=stexpr.getArg(0);
									String arg = stexpr.getArg(0).toString(); // $i1, 2 etc
									Type argType = stexpr.getArg(0).getType(); //int, boolean gives int (why?) primitive type
//									Type exprType = stexpr.getType(); //java.lang.Integer, java.lang.Boolean etc 
									if(argVal instanceof Local) //Means JimpleLocal (say $i2)
									{
										//Assuming type is java.lang.Integer or Boolean, no Double
										//Local assignment Constraints(needed for Path Constraints)
										rightExpr = ctx.mkIntConst(arg+"_"+localWriteCount.get(arg));
										System.out.println("IE: JimpleLocal");
									}
									else //Means Constant (say 2)
									{										
										//Assuming type is java.lang.Integer or Boolean, no Double										
										//Local assignment Constraints(needed for Path Constraints)
										rightExpr = ctx.mkInt(Integer.parseInt(arg));
										System.out.println("IE: Constant of type"+argType);
									}
								}
								
								//Static invoke of function call in a package
								if (stexpr.getMethod().getSignature().contains(testcase+".Main"))
			                	{
									List<Expr> localParams = new ArrayList<Expr>();
									
									for (Value arg : stexpr.getArgs()) {
										if (arg instanceof Local)
										{
											if(localWriteCount.get(arg)==null)
											{
//												localWriteCount.put(arg.toString(), 1);
												localParams.add(ctx.mkIntConst(arg.toString()+"_"+localWriteCount.get(arg.toString())));
											}
											else
											{
//												localWriteCount.put(arg.toString(), localWriteCount.get(arg.toString())+1);
												localParams.add(ctx.mkIntConst(arg.toString()+"_"+localWriteCount.get(arg.toString())));
											}
												
										}
										else
											localParams.add(ctx.mkInt(Integer.parseInt(arg.toString())));
									}
									
									//Walk this function
			                		SootMethod smethod = stexpr.getMethod();
			                		threadWalk(threadId,smethod,orderVariable,localParams); //Pass parameter list also this time
			                		int count = (counter.get(threadId)-1);
			                		prevOrderVariable= "O_"+threadId+"_"+count; //Because it will have been incremented before
			                		orderVariable=prevOrderVariable;
			                		System.out.println("PRINTSPecial"+prevOrderVariable);
									
									System.out.println(localParams);
									
									rightExpr = ctx.mkIntConst(returnValue);
			                	}
								
							}
						}
						
						if(((AssignStmt) s).getRightOp() instanceof Local)
						{
							String rightOp = ((AssignStmt) s).getRightOp().toString()+"_"+localWriteCount.get(((AssignStmt) s).getRightOp().toString());
							rightExpr = ctx.mkIntConst(rightOp);
							
						}
						if(((AssignStmt) s).getRightOp() instanceof Constant)
						{
							rightExpr = ctx.mkInt(Integer.parseInt(((AssignStmt) s).getRightOp().toString()));
						}
						
						
						if(((AssignStmt) s).getRightOp() instanceof StaticInvokeExpr)
						{
							
							StaticInvokeExpr iexpr = (StaticInvokeExpr) ((AssignStmt) s).getRightOp();
							//For command line arguments
							if(((AssignStmt) s).getRightOp().toString().contains("parseInt"))
									{
										String argVal = ((AssignStmt) prev).getRightOp().toString();
										int input = 0; 
										int inputIndex = Integer.parseInt(argVal.substring(argVal.length() - 2,argVal.length() - 1));
										
										FileReader argFile=null;
										try {
											argFile = new FileReader(argsFile);
										} catch (FileNotFoundException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										Scanner argScanner = new Scanner(argFile);
										input = Integer.parseInt(argScanner.nextLine().split(" ")[inputIndex]);
										rightExpr = ctx.mkInt(input);
										argScanner.close();

									}
							
//							if(iexpr.getArgCount()==1 )
//							{
//								Value rightOpVal = iexpr.getArgs().get(0);
//								
//								if(rightOpVal instanceof Local)
//								{
//									rightExpr = ctx.mkIntConst(rightOpVal.toString()+"_"+localWriteCount.get(rightOpVal.toString()));
//								}
//								else
//								{
//									System.out.println(rightOpVal+" is a constant");
//									rightExpr = ctx.mkInt(Integer.parseInt(rightOpVal.toString()));
//								}
//								System.out.println("HHSHHJ");
//							}
						}

						
						//ITT: Shared Variable Read ($r1=static_int_a)
						if(((AssignStmt) s).getRightOp() instanceof StaticFieldRef && ((StaticFieldRef)((AssignStmt) s).getRightOp()).getType().toString().contains("java.lang"))
						{
							String sharedVar = ((StaticFieldRef)((AssignStmt) s).getRightOp()).getField().getName().toString();
							String sharedVarField = ((StaticFieldRef)((AssignStmt) s).getRightOp()).getField().toString();
							
							//Update read count
							if(staticReadCount.get(sharedVar)==null)
								staticReadCount.put(sharedVar, 1);
							else
								staticReadCount.put(sharedVar, staticReadCount.get(sharedVar)+1);
							
							//Symbolic Value
							String symVal=sharedVar+"_R"+staticReadCount.get(sharedVar);
							HashMap<Integer, String> temp = intraThreadTrace.get(index);
							if(temp == null)
								temp = new HashMap<Integer, String>();
							temp.put(counter.get(threadId), "<"+threadId+","+"Read"+","+sharedVarField+","+symVal+">");
							intraThreadTrace.put(index, temp);
							System.out.println("STATICFIELDREF"+((StaticFieldRef)((AssignStmt) s).getRightOp()).getType().toString().contains("java.lang"));
							//PO Constraints
							orderVariable = "O_"+threadId+"_"+counter.get(threadId);
							//Add PO to Z3
							ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
							ArithExpr o2 = ctx.mkIntConst(orderVariable);
							BoolExpr poConst = ctx.mkLt(o1,o2);
							solver.add(poConst);
							
							//Make right expr for adding to constraints
							rightExpr = ctx.mkIntConst(symVal);
							
							counter.put(threadId, counter.get(threadId)+1);
							
							orderVariables.add(o2); // Add order variable to main List for final evaluation
						}
						
						//Then initialize or increment left op (If RHS contains a local, then there will always be a value assigned before because locals are initialized first))
					
						String jimpleLocal = ((AssignStmt) s).getLeftOp().toString();

						if(localWriteCount.get(jimpleLocal)==null)
						{
							localWriteCount.put(jimpleLocal, 1); //Increment later if not initialized to 1
						}
						else
						localWriteCount.put(jimpleLocal, localWriteCount.get(jimpleLocal)+1);
						int writeCount = localWriteCount.get(jimpleLocal);
						System.out.println("WRITECOUNT"+jimpleLocal+"_"+writeCount);
						
						BoolExpr assignStmnt = ctx.mkEq(ctx.mkIntConst(jimpleLocal+"_"+writeCount), rightExpr);
						//Add final assignment of the form "local = RHS"
						solver.add(assignStmnt);
					}
					
					//ITT: Shared Variable Write
					if(((AssignStmt) s).getLeftOp() instanceof StaticFieldRef && ((StaticFieldRef)((AssignStmt) s).getLeftOp()).getField().toString().contains("java.lang")) 
					{
						String sharedVar = ((StaticFieldRef)((AssignStmt) s).getLeftOp()).getField().getName().toString();
						String sharedVarField = ((StaticFieldRef)((AssignStmt) s).getLeftOp()).getField().toString();
						
						//Update write count
						if(staticWriteCount.get(sharedVar)==null)
							staticWriteCount.put(sharedVar, 1);
						else
							staticWriteCount.put(sharedVar, staticWriteCount.get(sharedVar)+1);
						
						//Symbolic Value
						String symVal=sharedVar+"_W"+staticWriteCount.get(sharedVar);
						HashMap<Integer, String> temp = intraThreadTrace.get(index);
						if(temp == null)
							temp = new HashMap<Integer, String>();
						temp.put(counter.get(threadId), "<"+threadId+","+"Write"+","+sharedVarField+","+symVal+">");
						intraThreadTrace.put(index, temp);
						System.out.println("STATICFIELDREF"+((StaticFieldRef)((AssignStmt) s).getLeftOp()).getField().getName().toString());
						
						//PO Constraints
						orderVariable = "O_"+threadId+"_"+counter.get(threadId);
						
						//Add PO to Z3
						ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
						ArithExpr o2 = ctx.mkIntConst(orderVariable);
						BoolExpr poConst = ctx.mkLt(o1,o2);
						solver.add(poConst);
						//Local assignment Constraints(needed for Path Constraints)
						ArithExpr o3 = ctx.mkIntConst(symVal);
						ArithExpr o4 = null;
						//Create rightop on the fly using its latest write count (assuming it is a JimpleLocal)
						
						Value rightOpVal=((AssignStmt) s).getRightOp();
						if(rightOpVal instanceof Local)
						{
							String rightOp = ((AssignStmt) s).getRightOp().toString()+"_"+localWriteCount.get(((AssignStmt) s).getRightOp().toString());
							o4 = ctx.mkIntConst(rightOp);
						}
						else // if constant (rare though)
						{
							o4 = ctx.mkIntConst(((AssignStmt) s).getRightOp().toString());
						}
						
						BoolExpr pConst = ctx.mkEq(o3,o4);
						solver.add(pConst);
						counter.put(threadId, counter.get(threadId)+1);
						
						orderVariables.add(o2); // Add order variable to main List for final evaluation
					}
				}
				
				if(s instanceof InvokeStmt)
				{
					InvokeExpr iexpr =((InvokeStmt) s).getInvokeExpr();
					
					if (iexpr instanceof VirtualInvokeExpr)
					{
						//ITT: For Fork
						if(((VirtualInvokeExpr) iexpr).getMethod().getSignature().equals("<java.lang.Thread: void start()>"))
						{
							HashMap<Integer, String> temp = intraThreadTrace.get(index);
							if(temp == null)
								temp = new HashMap<Integer, String>();
							if(childsForked.get(threadId)==null)
								childsForked.put(threadId, 0);
							temp.put(counter.get(threadId), "<"+threadId+","+"Fork"+","+threadId+"."+childsForked.get(threadId)+">");
							String base = ((VirtualInvokeExpr)iexpr).getBase().toString(); //Base t1,t2 etc
							System.out.println("BASE"+base);
							if(baseToTid.get(base)==null)
								baseToTid.put(base, threadId+"."+childsForked.get(threadId));
							childsForked.put(threadId, childsForked.get(threadId)+1);
							intraThreadTrace.put(index, temp);
							//PO Constraints
							orderVariable = "O_"+threadId+"_"+counter.get(threadId);
							//Add PO to Z3
							ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
							ArithExpr o2 = ctx.mkIntConst(orderVariable);
							BoolExpr poConst = ctx.mkLt(o1,o2);
							
//							if(!prevOrderVariable.equals("O_DUMMY")&&!orderVariable.equals("O_DUMMY"))
							solver.add(poConst);
							counter.put(threadId, counter.get(threadId)+1);
							
							orderVariables.add(o2); // Add order variable to main List for final evaluation
						}
						
						//ITT: For Join
						if(((VirtualInvokeExpr) iexpr).getMethod().getSignature().equals("<java.lang.Thread: void join()>"))
						{							
							HashMap<Integer, String> temp = intraThreadTrace.get(index);
							if(temp == null)
								temp = new HashMap<Integer, String>();
							String base = ((VirtualInvokeExpr)iexpr).getBase().toString(); //Get value of base t1,t2 to tid
							temp.put(counter.get(threadId), "<"+threadId+","+"Join"+","+baseToTid.get(base)+">");
							intraThreadTrace.put(index, temp);
							//PO Constraints
							orderVariable = "O_"+threadId+"_"+counter.get(threadId);
							//Add PO to Z3
							ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
							ArithExpr o2 = ctx.mkIntConst(orderVariable);
							BoolExpr poConst = ctx.mkLt(o1,o2);
							solver.add(poConst);
							counter.put(threadId, counter.get(threadId)+1);
							
							orderVariables.add(o2); // Add order variable to main List for final evaluation
						}
						
						//For dynamic binding
						if(((VirtualInvokeExpr) iexpr).getMethod().getSignature().contains(testcase))
						{
//							String classname = ((VirtualInvokeExpr) iexpr).getBase().getType().toString();
							
							
							
							//Get main and run SootMethod to pass it initially
							
							String objectType = ((VirtualInvokeExpr)iexpr).getBase().getType().toString();
	                		String referenceSignature = ((VirtualInvokeExpr)iexpr).getMethod().getSignature().toString();
	                		//Split referenceSIgnature by : and replace left half with object Type and get appropriate soot method and pass it to threadWalk
	                		String[] splitSig = referenceSignature.split(":");
	                		String newSig = "<"+objectType+": "+splitSig[1].trim();
	                		
	                		System.out.println("Object Type"+((VirtualInvokeExpr)iexpr).getBase().getType().toString());
	                		System.out.println("SootMethod"+referenceSignature);
	                		System.out.println("SootMethod Signature"+referenceSignature);
	                		System.out.println("New Signature"+newSig);
	                		
	                		//Find appropriate SootMethod for this sign to pass as parameter to threadWalk
							Chain<SootClass> chain = Scene.v().getApplicationClasses(); //Gets all classes
							Iterator<SootClass> chainIt = chain.iterator();
							SootMethod func = null;
							
							while(chainIt.hasNext())
							{
								SootClass class1 = chainIt.next();
								
								//Debugging info
								System.out.println();
								System.out.println("CLASS"+class1);
								System.out.println("MethodsInClass"+class1.getMethods());
								System.out.println();
								//Debugging info
								List<SootMethod> methods= class1.getMethods();
								Iterator<SootMethod> methodsIt = methods.iterator();
								
								while(methodsIt.hasNext())
								{
									SootMethod myMethod = methodsIt.next();
									System.out.println("METHOD: "+myMethod+"s");
									if(myMethod.toString().equals(newSig))
									{
										func = myMethod;
										System.out.println("FOUND SIG "+ func);
									}	
									
								}
							}

	                		threadWalk(threadId,func,orderVariable,null);
	                		int count = (counter.get(threadId)-1);
	                		prevOrderVariable= "O_"+threadId+"_"+count; //Because it will have been incremented before
	                		orderVariable=prevOrderVariable;
	                		System.out.println("ORDERVARIABLE"+prevOrderVariable);
						}
					}
					
					if(iexpr instanceof InterfaceInvokeExpr)
					{
						//ITT: For Lock
						if(((InterfaceInvokeExpr) iexpr).getMethod().getSignature().equals("<java.util.concurrent.locks.Lock: void lock()>"))
						{
//							String base = ((InterfaceInvokeExpr) iexpr).getBase().toString();
							//Get lock object from previous statement
							String lockObject =((StaticFieldRef)((AssignStmt) prev).getRightOp()).getField().toString();
							System.out.println("PREV is"+prev);
							HashMap<Integer, String> temp = intraThreadTrace.get(index);
							if(temp == null)
								temp = new HashMap<Integer, String>();
							temp.put(counter.get(threadId), "<"+threadId+","+"Lock"+","+lockObject+">");
							intraThreadTrace.put(index, temp);
							//PO Constraints
							orderVariable = "O_"+threadId+"_"+counter.get(threadId);
							//Add PO to Z3
							ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
							ArithExpr o2 = ctx.mkIntConst(orderVariable);
							BoolExpr poConst = ctx.mkLt(o1,o2);
							solver.add(poConst);
							counter.put(threadId, counter.get(threadId)+1);;
							
							orderVariables.add(o2); // Add order variable to main List for final evaluation
						}
						
						//ITT: For Unlock
						if(((InterfaceInvokeExpr) iexpr).getMethod().getSignature().equals("<java.util.concurrent.locks.Lock: void unlock()>"))
						{
//							String base = ((InterfaceInvokeExpr) iexpr).getBase().toString();
							//Get lock object from previous statement
							String lockObject =((StaticFieldRef)((AssignStmt) prev).getRightOp()).getField().toString();
							HashMap<Integer, String> temp = intraThreadTrace.get(index);
							if(temp == null)
								temp = new HashMap<Integer, String>();
							temp.put(counter.get(threadId), "<"+threadId+","+"Unlock"+","+lockObject+">");
							intraThreadTrace.put(index, temp);
							//PO Constraints
							orderVariable = "O_"+threadId+"_"+counter.get(threadId);
							//Add PO to Z3
							ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
							ArithExpr o2 = ctx.mkIntConst(orderVariable);
							BoolExpr poConst = ctx.mkLt(o1,o2);
							solver.add(poConst);
							counter.put(threadId, counter.get(threadId)+1);
							
							orderVariables.add(o2); // Add order variable to main List for final evaluation
						}
					}
					
					// Function call to init has this type
					if (iexpr instanceof SpecialInvokeExpr) 
	                {
						//Any function call in this package (so far init)
	                	if (((SpecialInvokeExpr)iexpr).getMethod().getSignature().contains(testcase+".Main"))
	                	{
	                		SootMethod smethod = ((SpecialInvokeExpr)iexpr).getMethod();
	                		threadWalk(threadId,smethod,orderVariable,null);
	                		int count = (counter.get(threadId)-1);
	                		prevOrderVariable= "O_"+threadId+"_"+count; //Because it will have been incremented before
	                		orderVariable=prevOrderVariable;
	                		System.out.println("PRINTSPecial"+prevOrderVariable);
	                	}
	                }
	                if (iexpr instanceof StaticInvokeExpr)
	                {      
	                	//Any function call in this package
	                	if (((StaticInvokeExpr)iexpr).getMethod().getSignature().contains(testcase+".Main"))
	                	{
	                		//With one parameter
	                		if(((StaticInvokeExpr)iexpr).getArgCount()==1)
	                		{
	                			List<Expr> localParams = new ArrayList<Expr>();
								
								for (Value arg : ((StaticInvokeExpr)iexpr).getArgs()) {
									if (arg instanceof Local)
									{
										if(localWriteCount.get(arg)==null)
										{
//											localWriteCount.put(arg.toString(), 1);
											localParams.add(ctx.mkIntConst(arg.toString()+"_"+localWriteCount.get(arg.toString())));
										}
										else
										{
//											localWriteCount.put(arg.toString(), localWriteCount.get(arg.toString())+1);
											localParams.add(ctx.mkIntConst(arg.toString()+"_"+localWriteCount.get(arg.toString())));
										}
											
									}
										
									else
										localParams.add(ctx.mkInt(Integer.parseInt(arg.toString())));
								}
								//Walk this function
		                		SootMethod smethod = ((StaticInvokeExpr)iexpr).getMethod();
		                		threadWalk(threadId,smethod,orderVariable,localParams); //Pass parameter list also this time
		                		int count = (counter.get(threadId)-1);
		                		prevOrderVariable= "O_"+threadId+"_"+count; //Because it will have been incremented before
		                		orderVariable=prevOrderVariable;
		                		System.out.println("PRINTSPecial"+prevOrderVariable);
								
								System.out.println(localParams);
								
//								rightExpr = ctx.mkIntConst(returnValue);
	                		}
	                		else // With no parameters
	                		{
	                			SootMethod smethod = ((StaticInvokeExpr)iexpr).getMethod();
		                		threadWalk(threadId,smethod,orderVariable,null);
		                		int count = (counter.get(threadId)-1);
		                		prevOrderVariable= "O_"+threadId+"_"+count;
		                		orderVariable=prevOrderVariable;
		                		System.out.println("PRINTStatic"+prevOrderVariable);
	                		}
	                		
	                	}
	                	//End of Thread
	                    if (((StaticInvokeExpr)iexpr).getMethod().getSignature().equals("<java.lang.System: void exit(int)>"))
	                    {             
	    					if (method.getSignature().contains("void run()") || method.getSignature().contains("void main(java.lang.String[])"))
	    					{
		    					HashMap<Integer, String> temp = intraThreadTrace.get(index);
		    					if(temp == null)
		    						temp = new HashMap<Integer, String>();
		    					temp.put(counter.get(threadId), "<"+threadId+","+"End"+">");
		    					intraThreadTrace.put(index, temp);
		    					//PO Constraints
		    					orderVariable = "O_"+threadId+"_"+counter.get(threadId);
		    					//Add PO to Z3
		    					ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
		    					ArithExpr o2 = ctx.mkIntConst(orderVariable);
		    					BoolExpr poConst = ctx.mkLt(o1,o2);
		    					solver.add(poConst);
		    					counter.put(threadId, counter.get(threadId)+1);
		    					
		    					orderVariables.add(o2); // Add order variable to main List for final evaluation
	    					}
	    					
	                    }
	                }
				}
				
				//End of Thread
				if(s instanceof ReturnStmt || s instanceof ReturnVoidStmt)
				{
					if (method.getSignature().contains("void run()") || method.getSignature().contains("void main(java.lang.String[])"))
					{
						HashMap<Integer, String> temp = intraThreadTrace.get(index);
						if(temp == null)
							temp = new HashMap<Integer, String>();
						temp.put(counter.get(threadId), "<"+threadId+","+"End"+">");
						intraThreadTrace.put(index, temp);
						//PO Constraints
						orderVariable = "O_"+threadId+"_"+counter.get(threadId);
						//Add PO to Z3
						ArithExpr o1 = ctx.mkIntConst(prevOrderVariable);
						ArithExpr o2 = ctx.mkIntConst(orderVariable);
						BoolExpr poConst = ctx.mkLt(o1,o2);
						solver.add(poConst);
						counter.put(threadId, counter.get(threadId)+1);
						
						orderVariables.add(o2); // Add order variable to main List for final evaluation
					}
					if(method.getReturnType().toString().equals("java.lang.Integer"))
					{
						returnValue =((ReturnStmt) s).getOp().toString()+"_"+localWriteCount.get(((ReturnStmt) s).getOp().toString());
						System.out.println("RETURN VALUE IS "+returnValue);
					}
				
				}
				prevOrderVariable = orderVariable;
				prev=s;
			}
			
		}
		
		System.out.println("--------------------------------------Thread Walk for "+threadId+splitTuple[1]+splitTuple[2]+" ends--------------------------------------");
		
	}
	
	protected void generatePartialOrderConstraints() // Fork Join
	{
		System.out.println("----------------------------------Generating Partial Order Constraints------------------------------");
		Set<Entry<String, HashMap<Integer, String>>> ittSet =intraThreadTrace.entrySet();
		Iterator<Entry<String, HashMap<Integer, String>>> ittIt = ittSet.iterator();
		while(ittIt.hasNext())
		{
			Entry<String, HashMap<Integer, String>> ittEntry = ittIt.next();
				HashMap<Integer, String> ittValMap = ittEntry.getValue();
				Set<Entry<Integer, String>> ittValSet = ittValMap.entrySet();
				Iterator<Entry<Integer, String>> ittValIt = ittValSet.iterator();
				while(ittValIt.hasNext())
				{
					Entry<Integer, String> ittValEntry = ittValIt.next();
					String itt = ittValEntry.getValue();
					String threadId = ittValEntry.getValue().split(",")[0].substring(1);
					String line = ittValEntry.getKey().toString();
					String orderVariable = "O_"+threadId+"_"+line;
					if(itt.contains("Fork"))
					{
						String childId=ittValEntry.getValue().split(",")[2].replaceAll(">", "").trim();
//						System.out.println("CHILD ID IS"+childId);
						//Get ordervariable of childs begin statement
						String childOv = getOvFromTrace("<"+childId+","+"Begin"+">");
//						System.out.println("CHILD OV IS"+childOv);
						//Add PO to Z3
						ArithExpr o1 = ctx.mkIntConst(orderVariable);
						ArithExpr o2 = ctx.mkIntConst(childOv);
						BoolExpr poConst = ctx.mkLt(o1,o2);
						solver.add(poConst);
						
					}
					if(itt.contains("Join"))
					{
						String childId=ittValEntry.getValue().split(",")[2].replaceAll(">", "").trim();
//						System.out.println("CHILD ID IS"+childId);
						//Get ordervariable of childs begin statement
						String childOv = getOvFromTrace("<"+childId+","+"End"+">");
//						System.out.println("CHILD OV IS"+childOv);
						//Add PO to Z3
						ArithExpr o1 = ctx.mkIntConst(childOv);
						ArithExpr o2 = ctx.mkIntConst(orderVariable);
						BoolExpr poConst = ctx.mkLt(o1,o2);
						solver.add(poConst);
						
					}
				}
		}
	}
	protected void generateLockingConstraints() // Lock Unlock
	{
		System.out.println("----------------------------------Generating Locking Constraints------------------------------");
		
		//Add to lockOvSet all lock unlock pairs
		Set<Entry<String, HashMap<Integer, String>>> ittSet =intraThreadTrace.entrySet();
		Iterator<Entry<String, HashMap<Integer, String>>> ittIt = ittSet.iterator();
		while(ittIt.hasNext())
		{
			Entry<String, HashMap<Integer, String>> ittEntry = ittIt.next();
				HashMap<Integer, String> ittValMap = ittEntry.getValue();
				Set<Entry<Integer, String>> ittValSet = ittValMap.entrySet();
				Iterator<Entry<Integer, String>> ittValIt = ittValSet.iterator();
				while(ittValIt.hasNext())
				{
					Entry<Integer, String> ittValEntry = ittValIt.next();
					String itt = ittValEntry.getValue();
					String threadId = ittValEntry.getValue().split(",")[0].substring(1);
					String line = ittValEntry.getKey().toString();
					String orderVariable = "O_"+threadId+"_"+line;
					if(itt.split(",")[1].contentEquals("Lock"))
					{
//						System.out.println("Current LOCK LINE IS "+line);
						String lockObj=ittValEntry.getValue().split(",")[2].replaceAll(">", "").trim();
//						System.out.println("Current Lock  Object"+lockObj);
//						System.out.println("Corr lock OV IS "+orderVariable);
						for(int i: ittValMap.keySet()) //Find next unlock higher than this counter WITH SAME LOCK OBJECT (wont handle nested locks)
						{							
							
							if (ittValMap.get(i).contains("Unlock") && i>Integer.parseInt(line))
							{
								String unlockObj = ittValMap.get(i).split(",")[2].replaceAll(">", "").trim();
//								System.out.println("Corr UNlock LIne IS "+i);
//								System.out.println("Corr UNlock TRACE IS "+ittValMap.get(i));
//								System.out.println("Corr UNlock Obkect IS "+unlockObj);
								if(lockObj.equals(unlockObj))
								{
									String corrUnlockOv = "O_"+threadId+"_"+i;
//									System.out.println("Corr Unlock OV is "+corrUnlockOv);
//									System.out.println("TRUE");
									if(lockOvSet.get(lockObj) == null)
									{
										HashMap<String,String> temp = new HashMap<String,String>();
										temp.put(orderVariable, corrUnlockOv);
										lockOvSet.put(lockObj, temp);
									}
									else
									{
										HashMap<String,String> temp = new HashMap<String,String>();
										temp = lockOvSet.get(lockObj);
										temp.put(orderVariable, corrUnlockOv);
										lockOvSet.put(lockObj,temp);
									}
									break;
								}
								
							}
						}
					}
				}
		}
	System.out.println("LockOvSet "+lockOvSet);
	
	//Generate Lock/Unlock Constraints
	Set<Entry<String, HashMap<String, String>>> lockSet = lockOvSet.entrySet();
	Iterator<Entry<String, HashMap<String, String>>> lockSetIt = lockSet.iterator();
	while(lockSetIt.hasNext()) //Iterate over all locks
	{
		Entry<String, HashMap<String, String>> lockSetEntry = lockSetIt.next(); //For lock i
		HashMap<String, String> OvSet = lockSetEntry.getValue();
		//For this HashMap, generate constraints
		
		for(String ov:OvSet.keySet())
		{
			for(String ov1:OvSet.keySet())
			{
				if(!ov.equalsIgnoreCase(ov1)) //No need to compare with itself(same pair in thread) //Will cause error in test 13 two lock objects one after the other
				{
					//Will create duplicates. Try to generate code for nC2 subsets; should solve the problem
//					System.out.println("Lock1"+ov);
//					System.out.println("Unlock2"+OvSet.get(ov1));
//					System.out.println("Unlock1"+OvSet.get(ov));
//					System.out.println("Lock2"+ov1);
					String lock1 = ov;
					String unlock2 = OvSet.get(ov1);
					String lock2 = ov1;
					String unlock1 = OvSet.get(ov);
					BoolExpr exp1 = ctx.mkGt(ctx.mkIntConst(lock1), ctx.mkIntConst(unlock2));
					BoolExpr exp2 = ctx.mkGt(ctx.mkIntConst(lock2), ctx.mkIntConst(unlock1));
					BoolExpr exp3 = ctx.mkOr(exp1,exp2);
					solver.add(exp3);
				}
				
			}
			
		}
	}
	}
	protected void generateReadWriteConstraints() // Read Write
	{
		System.out.println("----------------------------------Read Write Constraints------------------------------");
		
		//Record all read writes in the appropriate data structures
		Set<Entry<String, HashMap<Integer, String>>> ittSet =intraThreadTrace.entrySet();
		Iterator<Entry<String, HashMap<Integer, String>>> ittIt = ittSet.iterator();
		while(ittIt.hasNext())
		{
			Entry<String, HashMap<Integer, String>> ittEntry = ittIt.next();
				HashMap<Integer, String> ittValMap = ittEntry.getValue();
				Set<Entry<Integer, String>> ittValSet = ittValMap.entrySet();
				Iterator<Entry<Integer, String>> ittValIt = ittValSet.iterator();
				while(ittValIt.hasNext())
				{
					Entry<Integer, String> ittValEntry = ittValIt.next();
					String itt = ittValEntry.getValue();
					String threadId = ittValEntry.getValue().split(",")[0].substring(1);
					String line = ittValEntry.getKey().toString();
					String orderVariable = "O_"+threadId+"_"+line;
					if(itt.contains("Read"))
					{
						String sharedVar=ittValEntry.getValue().split(",")[2].trim();
						String symSharedVal = ittValEntry.getValue().split(",")[3].replaceAll(">", "").trim();
//						System.out.println("SYMVAL"+symSharedVal);
//						System.out.println("Read"+sharedVar+"  "+orderVariable);
						if(sharedVarRead.get(sharedVar)==null)
						{
							HashMap<String,String> temp = new HashMap<String,String>();
							temp.put(symSharedVal, orderVariable);
							
							sharedVarRead.put(sharedVar, temp);
						}
						else
						{
							sharedVarRead.get(sharedVar).put(symSharedVal, orderVariable);
						}
						
					}
					if(itt.contains("Write"))
					{
						String sharedVar=ittValEntry.getValue().split(",")[2].trim();
						String symSharedVal = ittValEntry.getValue().split(",")[3].replaceAll(">", "").trim();
//						System.out.println("SYMVAL"+symSharedVal);
//						System.out.println("Write"+sharedVar+"  "+orderVariable);
						if(sharedVarWrite.get(sharedVar)==null)
						{
							HashMap<String,String> temp = new HashMap<String,String>();
							temp.put(symSharedVal, orderVariable);
							
							sharedVarWrite.put(sharedVar, temp);
						}
						else
						{
							sharedVarWrite.get(sharedVar).put(symSharedVal, orderVariable);
						}
					}
				}
		}
		System.out.println("sharedVarRead"+sharedVarRead);
		System.out.println("sharedVarWrite"+sharedVarWrite);
		
		//Generate Constraints
		
		//Algo:
		//For each Shared Variable, do
		//For each read of a Shared Variable, do
		//For each write that can be assigned to a read, do
		//For all writes not equal to the above write, do
		
		BoolExpr readWriteConstraints = ctx.mkBool(false); //Final BoolExp(SHOULD BE FALSE BUT USE TRUE FOR DEBUGGING)
		if(sharedVarRead!=null) // IF NO READS
		{
			for(String shvar: sharedVarRead.keySet()) // For each read shared variable(Works for all variables)
			{
				BoolExpr innerExpr = null;
				HashMap<String,String> shvarRead = sharedVarRead.get(shvar);
//				System.out.println("SHVARRead"+shvarRead);
				for(String shvarRd: shvarRead.keySet()) //For each read of a Shared Variable
				{
					String Vr = shvarRd;
					String Or=null;
					
//					if(shvarRead.get(Vr)!=null)
					 Or = shvarRead.get(Vr);
					//Get Write map for this Shared Variable
					if(sharedVarWrite!=null) //IF NO WRITES
					{
						
						HashMap<String,String> shvarWrite=sharedVarWrite.get(shvar); //Get writes map for that shared variable
//						System.out.println("SHVARWRITEKEY"+shvarWrite.keySet());
						
						for(String shvarWt: shvarWrite.keySet()) // For each write that can be assigned to a read 
						{
							String wi = shvarWt;
							String Owi = shvarWrite.get(wi);
							innerExpr = ctx.mkEq(ctx.mkIntConst(Vr), ctx.mkIntConst(wi));
							innerExpr = ctx.mkAnd(innerExpr,ctx.mkLt(ctx.mkIntConst(Owi),ctx.mkIntConst(Or)));
							for(String shvarWt2: shvarWrite.keySet()) //For all writes not equal to the above write
							{
								if(!(shvarWt.equals(shvarWt2)))
								{
									String wj = shvarWt2;
//									String Owj=null;
//									if(shvarWrite.get(wj)!=null)
									String Owj = shvarWrite.get(wj);
//									System.out.println("TESTEQ"+shvarWt+shvarWt2);
									
									//Encode Constraints Owj<Owi OR Owj>Or
									
									ArithExpr AOwj = ctx.mkIntConst(Owj);
//									if(shvarWrite.get(wj)!=null && shvarRead.get(Vr)!=null)
										innerExpr = ctx.mkAnd(innerExpr, ctx.mkOr(ctx.mkLt(AOwj, ctx.mkIntConst(Owi)),ctx.mkGt(AOwj, ctx.mkIntConst(Or))));
								}
							}
											
							readWriteConstraints = ctx.mkOr(readWriteConstraints,innerExpr);
//							System.out.println(Vr+" EQUAL  "+wi);
						}
					}
//					readWriteConstraints = ctx.mkBool(arg0);
					solver.add(readWriteConstraints);
					readWriteConstraints=ctx.mkBool(false);
					innerExpr=null;
				}
				
			}
			
		}
		
		 //Final ReadWriteConstraints
	}
	//FoolProof 
	protected void printGlobalTrace() throws IOException
	{
		String globalTraceOutPath = "Testcases/" + project + "/processed-output/" + testcase + ".global_trace";
		PrintWriter globalTraceWriter = new PrintWriter(globalTraceOutPath);
		
		int min=0;
		for(int i =0;i<orderVariablesAssignment.size();i++)
		{
			int ova = orderVariablesAssignment.get(i);
			if(ova<min)
			{
				min=ova; 
			}
			
		}
		
		//Start from min assignment and keep printing values
		
		while(true)
		{
			int location = containsUnprintedMinAtLocation(min);
			if(location == -100) // All true
				break;
			if(location == -200) // If reached the end and more elements left to be processed
				{
					min++;
					location = containsUnprintedMinAtLocation(min);
					continue;
				}
			
			//Get order variables orderVariables and print them onto the final output global-trace
			
			String intraThreadTrace = getTraceFromOv(orderVariables.get(location).toString());
			intraThreadTrace=intraThreadTrace.substring(1, intraThreadTrace.length()-1).replaceAll(",", ", ").trim();
			globalTraceWriter.println(intraThreadTrace);
			System.out.println(intraThreadTrace);
			orderVariablesPrinted.set(location, 1); // Mark that location as true, Use .set instead of .add. .add adds new values and increases the size of arraylist
			System.out.println("TEST"+" "+ orderVariables.get(location) + " "+min+"  "+location);
			if(containsUnprintedMinAtLocation(min)==-100)
			{
				min++;
			}

		}
      
		globalTraceWriter.close();
	}
	
	protected int containsUnprintedMinAtLocation(int min)
	{
		int location = -100;
				for(int i=0;i<orderVariablesAssignment.size();i++)
				{
					if(orderVariablesAssignment.get(i).equals(min) && orderVariablesPrinted.get(i).equals(0))
					{
						location = i;
						break;
					}
					//If reached the end and there are still variables left to be processed
					else if(i==orderVariablesAssignment.size()-1 && orderVariablesPrinted.contains(0)) 
					{
						location = -200;
					}
				}
				//location will be -100 if reached the end and no elements left to be processed
		return location;
	}
	//Scans tuples file and gets block ids for the given tid and method signature
	protected String getTuple(String tid, String signature, Integer count){
		
		String tupleInPath = "Testcases/" + project + "/processed-output/" + testcase + ".tuples";
		String tuple=null;
		FileReader tupleIn; 
		try {
			tupleIn = new FileReader(tupleInPath);
			Scanner tupleScanner = new Scanner(tupleIn);
			while(tupleScanner.hasNextLine())
			{
				tuple = tupleScanner.nextLine();
				String[] splitTuple = tuple.split(","); 
				int count1 = Integer.parseInt(splitTuple[2]);
				if(splitTuple[0].contentEquals(tid) && splitTuple[1].contains(signature) &&count1==count)
				{
					break;
				}
			}
			tupleScanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tuple;
	}
	
	protected String getTraceFromOv(String ov)

	{
		Set<Entry<String, HashMap<Integer, String>>> ittSet =intraThreadTrace.entrySet();
		Iterator<Entry<String, HashMap<Integer, String>>> ittIt = ittSet.iterator();
		String itt = null;
		while(ittIt.hasNext())
		{
			Entry<String, HashMap<Integer, String>> ittEntry = ittIt.next();
				HashMap<Integer, String> ittValMap = ittEntry.getValue();
				Set<Entry<Integer, String>> ittValSet = ittValMap.entrySet();
				Iterator<Entry<Integer, String>> ittValIt = ittValSet.iterator();
				while(ittValIt.hasNext())
				{
					Entry<Integer, String> ittValEntry = ittValIt.next();
					String threadId = ittValEntry.getValue().split(",")[0].substring(1);
					String line = ittValEntry.getKey().toString();
					String orderVariable = "O_"+threadId+"_"+line;
					if(ov.contentEquals(orderVariable.trim()))
					{
						itt=ittValEntry.getValue();
						break;
					}
				}
		}
		return itt;
	}
	
	protected String getOvFromTrace(String trace)

	{
		Set<Entry<String, HashMap<Integer, String>>> ittSet =intraThreadTrace.entrySet();
		Iterator<Entry<String, HashMap<Integer, String>>> ittIt = ittSet.iterator();
		String ov = null;
		while(ittIt.hasNext())
		{
			Entry<String, HashMap<Integer, String>> ittEntry = ittIt.next();
				HashMap<Integer, String> ittValMap = ittEntry.getValue();
				Set<Entry<Integer, String>> ittValSet = ittValMap.entrySet();
				Iterator<Entry<Integer, String>> ittValIt = ittValSet.iterator();
				while(ittValIt.hasNext())
				{
					Entry<Integer, String> ittValEntry = ittValIt.next();
					String itt = ittValEntry.getValue();
					String threadId = ittValEntry.getValue().split(",")[0].substring(1);
					String line = ittValEntry.getKey().toString();
					String orderVariable = "O_"+threadId+"_"+line;
					if(trace.contentEquals(itt.trim()))
					{
						ov=orderVariable;
						break;
					}
				}
		}
		return ov;
	}
	protected void printIntraThreadTrace()
	{
		Set<Entry<String, HashMap<Integer, String>>> ittSet =intraThreadTrace.entrySet();
		Iterator<Entry<String, HashMap<Integer, String>>> ittIt = ittSet.iterator();
		while(ittIt.hasNext())
		{
			Entry<String, HashMap<Integer, String>> ittEntry = ittIt.next();
			System.out.println();
			System.out.println("Key: "+ittEntry.getKey());
			System.out.println("Value: ");
			System.out.println();
				HashMap<Integer, String> ittValMap = ittEntry.getValue();
				Set<Entry<Integer, String>> ittValSet = ittValMap.entrySet();
				Iterator<Entry<Integer, String>> ittValIt = ittValSet.iterator();
				while(ittValIt.hasNext())
				{
					Entry<Integer, String> ittValEntry = ittValIt.next();
					System.out.println("Line: "+ittValEntry.getKey());
					System.out.println("ITT: "+ittValEntry.getValue());
				}
			System.out.println();
		}
		
		
	}

}
