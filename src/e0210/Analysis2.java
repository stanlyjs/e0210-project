package e0210;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.io.PrintWriter;

/*
 * @author Stanly John Samuel	-		stanly.samuel@csa.iisc.ernet.in
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedMultigraph;
import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.LongType;
import soot.Modifier;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.*;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.util.Chain;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;

public class Analysis2 extends BodyTransformer {
	    		
	static SootClass hooksClass;
	static SootMethod printCounter, initCounter, incCounter,insertOnBackEdge, printNewLine;

	static {
	    hooksClass = Scene.v().loadClassAndSupport("e0210.Hooks");
	    initCounter = hooksClass.getMethod("void initializeCounter(int)");
	    incCounter = hooksClass.getMethod("void incrementCounter(int)");
	    printCounter = hooksClass.getMethod("void printCounter()");
	    insertOnBackEdge = hooksClass.getMethod("void insertOnBackEdge(int,int)");
	    printNewLine = hooksClass.getMethod("void printNewLine()");
	  }
	
	static int globalCounter; //Used for multiple function invocations.
	
	DirectedPseudograph<Block, DefaultEdge> graphP = new DirectedPseudograph<Block, DefaultEdge>(DefaultEdge.class);
	
	@Override
	protected synchronized void internalTransform(Body b, String phaseName, Map<String, String> options) {
		
		//Abhinav's Code
//		ExceptionalBlockGraph cfg = new ExceptionalBlockGraph(b);
//
//		for (Block block : cfg.getBlocks()) {
//			graphP.addVertex(block);
//		}
//
//		for (Block block : cfg.getBlocks()) {
//			for (Block succ : cfg.getSuccsOf(block))
//				graphP.addEdge(block, succ);
//		}
		
		//Ignore Hooks Class while Instrumenting
		if(b.getMethod().getDeclaringClass().getName().equals("e0210.Hooks"))
			{
				return;
			}
		
		//Local Declarations
		
		Local refLocal = Jimple.v().newLocal("refLocal", RefType.v("java.io.PrintStream"));
        b.getLocals().add(refLocal);
		Local counter = Jimple.v().newLocal("tmpCounter", LongType.v()); 
        b.getLocals().add(counter);
  		
        Set<DefaultWeightedEdge> backEdgeSet = new HashSet<DefaultWeightedEdge>();//used in createDirectedGraph to add backedges and in instrumentUsingBL TO INSTRUMENT CODE OF BACKEDGES USING EXCEPTION GRAPH
			
		//Execution begins here
        
        // 1. Do basic instrumentation
        instrumentBasic(b,counter,refLocal);
        
        //Then initialize graph on the modified 'b' because it depends on it for some reason
        ExceptionalBlockGraph graph = new ExceptionalBlockGraph(b);
        System.out.println(graph);
		DefaultDirectedWeightedGraph<String, DefaultWeightedEdge>  dg = new DefaultDirectedWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
		
        // 2. Create Directed Graph 'dg' from given Exceptional Block Graph 'graph'
		createDirectedGraph(graph,dg,backEdgeSet);
		System.out.println(dg);
		int totalPaths=0;
		// 3. Instrument using Ball Larus
		totalPaths = instrumentUsingBL(graph,dg,backEdgeSet,counter,refLocal,totalPaths,b);

		// 4. Generate path with ID and store it in a temporary file BLMapping.
		generatePath(dg,totalPaths);
		
		// 5. Update Global Counter for the next method in line.
		globalCounter = globalCounter+totalPaths; 
		System.out.println(b);
	}
	
	void instrumentBasic(Body b, Local counter, Local refLocal){
		
		boolean isFirstNonIdentityStmt = true;
		
        Iterator<Unit> stmtIt = b.getUnits().snapshotIterator();
        while(stmtIt.hasNext())
        {
            Stmt s = (Stmt) stmtIt.next();
            
            //Insert counter = globalCounter after all Identity statements in a method.
            if (!(s instanceof IdentityStmt) && isFirstNonIdentityStmt)
            {
            	//b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(initCounter.makeRef(),IntConstant.v(globalCounter))),s);
            	b.getUnits().insertBefore(Jimple.v().newAssignStmt(counter, LongConstant.v(globalCounter) ), s);
            	isFirstNonIdentityStmt = false;
            } 
            
            if (s instanceof InvokeStmt)
            {
                InvokeExpr iexpr = (InvokeExpr) ((InvokeStmt)s).getInvokeExpr();
                if (iexpr instanceof StaticInvokeExpr)
                {                      
                    if (((StaticInvokeExpr)iexpr).getMethod().getSignature().equals("<java.lang.System: void exit(int)>"))
                    {                        	
                    	//Print counter value
                    	
//                    	b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(printCounter.makeRef())), s);

        	    			// refLocal = java.lang.System.out;
        	               b.getUnits().insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
        	                	                
        	               // refLocal.println(counter);                        
        	                b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal,Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(long)").makeRef(),counter)), s);
    		                
                }
            }
            }
            else if (s instanceof ReturnStmt || s instanceof ReturnVoidStmt)
            {
            	//Print counter
            	
            	//Using hooks
//            		b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(printCounter.makeRef())), s);

	    			// refLocal = java.lang.System.out;
            		b.getUnits().insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
	                	                
	                // refLocal.println(counter);                        
            		b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal,Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(long)").makeRef(),counter)), s);

            	//Print new line
            		// refLocal = java.lang.System.out;
            		b.getUnits().insertBefore(Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())), s);
	                	                
	                // refLocal.println();                        
            		b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal,Scene.v().getSootClass("java.io.PrintStream").getMethod("void println()").makeRef())), s);

//            		b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(printNewLine.makeRef())), s);

            }
        }
	}
	
	//Function to create directed graph from given exceptional block graph Offline
	void createDirectedGraph(ExceptionalBlockGraph graph, DefaultDirectedWeightedGraph<String, DefaultWeightedEdge> dg,Set<DefaultWeightedEdge> backEdgeSet){

		// Create a Linked List store reverse topological order of graph
		List<Block> blocksList = graph.getBlocks();
		System.out.println("BLocksList"+blocksList);
		//Add vertices to graph dg from exception graph
		int i=0;
		while(i<blocksList.size())
		{
		Block b1 = blocksList.get(i);
		int x = b1.getIndexInMethod();
		String y = Integer.toString(x);
		dg.addVertex(y);
		System.out.println(x);
		i++;
		}
		
		//Add edges to graph dg from exception graph
		i=0;
		while(i<blocksList.size())
		{
		Block b1 = blocksList.get(i);        
		int x1 = b1.getIndexInMethod();
		String y1 = Integer.toString(x1);
		List<Block> subBlocksList = b1.getSuccs();
		
		int j=0;
		while(j<subBlocksList.size())
		{
			Block b2=subBlocksList.get(j);
			int x2 = b2.getIndexInMethod();
			String y2 = Integer.toString(x2);	
			dg.addEdge(y1,y2);

			j++;
		}
		i++;
		}
		
		//Only if graph contains a cycle
		CycleDetector cd = new CycleDetector(dg);
		Boolean containsCycles = cd.detectCycles();
		System.out.println("dg has Cycle?"+containsCycles);
		//Insert Node -1 and 1000000 and attach 1 to -1 and all leaf nodes to 1000000
		//if(containsCycles)
		//{
			dg.addVertex("-1");
			dg.addEdge("-1","0");
			dg.addVertex("1000000");
			//attach all leaf edges to 1000000
			Set<String> vs = dg.vertexSet();
			Iterator vsIterator = vs.iterator();
			while(vsIterator.hasNext())
			{
				String vertex = vsIterator.next().toString();
				if(dg.outDegreeOf(vertex)==0 && vertex!="1000000")
				{
					dg.addEdge(vertex,"1000000");
				}
			}

			//Add all backedges in dg in backEdgeSet
			
			Set<DefaultWeightedEdge> es = dg.edgeSet();
			Iterator esIterator = es.iterator();
			while(esIterator.hasNext())
			{
				DefaultWeightedEdge e = (DefaultWeightedEdge) esIterator.next();
				String source = dg.getEdgeSource(e);
				
				String target = dg.getEdgeTarget(e);
				int sourceInt = Integer.parseInt(source);
				int targetInt = Integer.parseInt(target);
				if(sourceInt>=targetInt) // (> for general loops, = for self loops)
				{
					backEdgeSet.add(e); // Add in a different Set because modifying current iterator collection affects the loop.
					
				}
			}
			//Replace all backedges given in backEdgeSet with add dummy edges.
			Iterator besIterator = backEdgeSet.iterator();
			while(besIterator.hasNext())
			{
				DefaultWeightedEdge e = (DefaultWeightedEdge) besIterator.next();
				String source = dg.getEdgeSource(e);
				String target = dg.getEdgeTarget(e);
				dg.addEdge("-1", target);
				dg.addEdge(source,"1000000");
				dg.removeEdge(e);
			}
			
			//Handling System.exit(0);
			
			//Traverse Exception graph and for those block IDs, remove currently assigned outgoing edges and add edge( vertex, 1000000) in dg.
			
			Set<String> vset = dg.vertexSet();
			Set<DefaultWeightedEdge> exitSet = new HashSet<DefaultWeightedEdge>();
			Iterator<String> vi = vset.iterator();
			while(vi.hasNext())
			{
				int vno = Integer.parseInt(vi.next().toString());
				if (vno!=-1 && vno !=1000000)
				{
					Block bl=graph.getBlocks().get(vno);
					Iterator<Unit> stmtIt=bl.iterator();
		
					while(stmtIt.hasNext())
				    {
				        Stmt s = (Stmt) stmtIt.next();
				            if (s instanceof InvokeStmt)
				            {
				                InvokeExpr iexpr = (InvokeExpr) ((InvokeStmt)s).getInvokeExpr();
				                if (iexpr instanceof StaticInvokeExpr)
				                {                        
				                    if (((StaticInvokeExpr)iexpr).getMethod().getSignature().equalsIgnoreCase("<java.lang.System: void exit(int)>"))
				                    {          
				                    	//Remove all outgoing edges
				                    	String vertex=Integer.toString(vno);
				                    	Set<DefaultWeightedEdge> outedges = dg.outgoingEdgesOf(vertex);
				                    	//Add all outgoing edges to exitSet
				                    	Iterator<DefaultWeightedEdge> outIterator = outedges.iterator();
				                    	while(outIterator.hasNext())
				                    	{
				                    		DefaultWeightedEdge e = outIterator.next();
				                    		exitSet.add(e);
				                    	}
				                    	//Remove all outgoing edges by comparing with exitSet
				                    	Iterator<DefaultWeightedEdge> exitIterator = exitSet.iterator();
				                    	while(exitIterator.hasNext())
				                    	{
				                    		DefaultWeightedEdge e = exitIterator.next();
				                    		dg.removeEdge(e);
				                    	}
				                    	//Attach this node t0 dummy node so that its path is not considered in Ball Larus.
				                    	dg.addEdge(vertex, "1000000");
				                    }
				                }
				            }
				    }
				}
			}
	}
		

	//Instrument the Exceptional Block Graph using Ball Larus and return the total paths in the method.
	int instrumentUsingBL(ExceptionalBlockGraph graph, DefaultDirectedWeightedGraph<String, DefaultWeightedEdge> dg,Set<DefaultWeightedEdge> backEdgeSet, Local counter,Local refLocal, int totalPaths, Body b)
	{
		TopologicalOrderIterator to = new TopologicalOrderIterator(dg);
		LinkedList<String> revTo= new LinkedList<String>();
		while(to.hasNext())
		{			
			String x = to.next().toString();
			revTo.addFirst(x);
		}
		System.out.println("REV TOP ORDER LINKEDLIST"+revTo);
		
		//Ball Larus Algorithm + Instrumentation OF ALL EDGES EXCEPT BACKEDGES
		HashMap numPathsHash = new HashMap();
		while(!revTo.isEmpty())
		{
			String x = revTo.removeFirst().toString();
			Set<DefaultWeightedEdge> outEdges=dg.outgoingEdgesOf(x);
			int index = Integer.parseInt(x);
			if (outEdges.isEmpty()) //Means it is a leaf node
			{
				numPathsHash.put(index, 1);
			}
			else //Non leaf node
			{
				numPathsHash.put(index, 0);
				Iterator<DefaultWeightedEdge> oeIterator = outEdges.iterator();
				while(oeIterator.hasNext())
				{
					DefaultWeightedEdge e = oeIterator.next();
					dg.setEdgeWeight(e,(int)numPathsHash.get(index));
					
					//INSTRUMENT the edge if weight not equal to zero AND if it is not a dummy vertex
					//For dummy source vertex (-1) all outgoing edges will NOT be instrumented(in FINAL code) for sure. Hence, index!=-1 makes sense.
					//However, I failed to consider that for dummy target vertex(1000000) which will never have any outgoing edges. 
					//Thus adding index!=1000000 does'nt make sense because it will never have any target vertex and hence 
					//it will try to instrument on edge conatining 1000000 in exception graph which will lead to an error 
					//In fact I have to check all outgoing edges of other vertices != 1000000
					if((int)numPathsHash.get(index)!=0 && index!=-1 && Integer.parseInt(dg.getEdgeTarget(e))!=1000000 ){
						String e1 = dg.getEdgeSource(e);
						System.out.println("SOURCE"+e1);
						
						String e2 = dg.getEdgeTarget(e);
						System.out.println("DESTINATION"+e2);
						
						//Get block from exceptionGraph corresponding to directed graph number
						int b1 = Integer.parseInt(e1);
						Unit tail = graph.getBlocks().get(b1).getTail();
						
						int b2 = Integer.parseInt(e2);
						Unit head = graph.getBlocks().get(b2).getHead();
					
	                    //counter = counter + numPaths[index];
//						b.getUnits().insertOnEdge(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(incCounter.makeRef(),IntConstant.v((int)numPathsHash.get(index)))), tail, head);
						b.getUnits().insertOnEdge(Jimple.v().newAssignStmt(counter, Jimple.v().newAddExpr(counter, LongConstant.v((int)numPathsHash.get(index)))), tail, head);
	          
					}
					
					int weight = (int) dg.getEdgeWeight(e);
					int target = Integer.parseInt(dg.getEdgeTarget(e));
					numPathsHash.put(index, (int)numPathsHash.get(index)+(int)numPathsHash.get(target));
					System.out.println("WEIGHT"+weight);
					System.out.println(e);
				}
			}
			System.out.println(outEdges);
			totalPaths=(int)numPathsHash.get(index);
		}
		
		//Instrument all backedges in ExceptionGraph by using backedgeSet as reference.
		
		Iterator besIteratorEGraph = backEdgeSet.iterator();
		while(besIteratorEGraph.hasNext())
		{
			DefaultWeightedEdge e = (DefaultWeightedEdge) besIteratorEGraph.next();
			String source = dg.getEdgeSource(e);
			String target = dg.getEdgeTarget(e);
			System.out.println("TIME TO INSTRUMENT"+e);
			
			//Get block from exceptionGraph corresponding to directed graph number
			int b1 = Integer.parseInt(source);
			Unit tail = graph.getBlocks().get(b1).getTail();
			System.out.println("TAIL"+tail);
			int b2 = Integer.parseInt(target);
			Unit head = graph.getBlocks().get(b2).getHead();
			System.out.println("HEAD"+head);
			
			//get weight(source,1000000)
			int w1=(int)dg.getEdgeWeight(dg.getEdge(source, "1000000"));
			System.out.println("W1"+w1);
			
			//counter = counter + weight(source,1000000);
			AssignStmt s1 = Jimple.v().newAssignStmt(counter, Jimple.v().newAddExpr(counter, LongConstant.v(w1)));
//						System.out.println("BODY"+b);
			//Print counter
        	
				// refLocal = java.lang.System.out;
						AssignStmt s2 = Jimple.v().newAssignStmt(refLocal, Jimple.v().newStaticFieldRef(Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef()));
	            	                
	            // refLocal.println(counter);                        
						InvokeStmt s3 = Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(refLocal,Scene.v().getSootClass("java.io.PrintStream").getMethod("void println(long)").makeRef(),counter));

			
			//get weight(-1,tail)
	    		int w2=(int)dg.getEdgeWeight(dg.getEdge("-1", target))+globalCounter; //Only need to increment this because it is reinitialized. The other counter above comes from counter=globalCounter
	    		System.out.println("W1"+w2);
			//counter = weight(-1,tail)+globalCounter;
	    		AssignStmt s4 = Jimple.v().newAssignStmt(counter,LongConstant.v(w2));
	    		
	    	//Create chain of statements
	    		ArrayList<Unit> chain= new ArrayList<Unit>();
	    		chain.add(s1);
	    		chain.add(s2);
	    		chain.add(s3);
	    		chain.add(s4);
//	    	Insert above statements chain on edge
	    		b.getUnits().insertOnEdge(chain,tail, head);
//	    		b.getUnits().insertOnEdge(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(insertOnBackEdge.makeRef(),IntConstant.v(w1),IntConstant.v(w2))),tail, head);
		}
		return totalPaths;
	}
	
	//Function to generate path IDs and map it to a file.
	void generatePath (DefaultDirectedWeightedGraph<String, DefaultWeightedEdge> dg, int totalPaths){
		String x;
		if(dg.containsVertex("-1")) // Root can only be 0 or -1
		{x ="-1";} 
		else
		{x = "0";}	
		File f = new File("BLMapping");
		
		try 
		{			
			
			PrintWriter pw = new PrintWriter(new FileOutputStream(f,true));
			if(globalCounter==0)
			{
				FileWriter fw = new FileWriter("BLMapping");
				PrintWriter pw1 = new PrintWriter(fw);
				pw1.write("");
				pw1.flush(); 
				pw1.close();
			}
			System.out.println("PAThGC"+globalCounter);
			for(int pathId = globalCounter; pathId<(globalCounter+totalPaths);pathId++)
			{
				pw.print(pathId+";");
				int temp = pathId;
				while(true)
					{
					Set<DefaultWeightedEdge> outEdges=dg.outgoingEdgesOf(x);
					
						if (outEdges.isEmpty() ) //Means it is a leaf node (can cause error for System.exit or in cases where leaf is not last node )
						{
							if(!x.matches("1000000"))
							{pw.print(x);}
							pw.println();
							if(pathId==globalCounter+totalPaths-1)
								{pw.println();}
							break; //ASSUMING LEAF IS LAST NODE! MAY CHANGE FOR CYCLES. DUNNO
						}
						else //Non leaf node
						{
							Iterator<DefaultWeightedEdge> oeIterator = outEdges.iterator();
							int weight,maxOutWeight=0;
							DefaultWeightedEdge finalOutEdge = new DefaultWeightedEdge();
							//Find maximum weight of out edge less than pathId
							while(oeIterator.hasNext())
							{
								DefaultWeightedEdge e = oeIterator.next();
								
								weight = (int) dg.getEdgeWeight(e)+globalCounter;
	//							System.out.println("Edge is: " 	 +" and Weight is: "+weight);
								if(weight<=temp)
								{
									maxOutWeight=weight-globalCounter;
									finalOutEdge = e;
								}
							}			
							temp=temp-maxOutWeight;
							if(!dg.getEdgeSource(finalOutEdge).matches("-1")) //DOnt print -1
							{pw.print(dg.getEdgeSource(finalOutEdge)+",");} //No problem with commas
						
							x=dg.getEdgeTarget(finalOutEdge); //update x with successor node
						} // end of if-else
					}//end of while
			
				if(dg.containsVertex("-1")) // Root can only be 0 or -1
				{x ="-1";} 
				else
				{x = "0";}	 //Reinitialize root for every path
			
			}//end of for
		pw.close(); //Close the stream
		
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Added By Abhinav
//	public void finish(String testcase) {
//
//		VertexNameProvider<Block> id = new VertexNameProvider<Block>() {
//
//			@Override
//			public String getVertexName(Block b) {
//				return String.valueOf("\"" + b.getBody().getMethod().getNumber() + " " + b.getIndexInMethod() + "\"");
//			}
//		};
//
//		VertexNameProvider<Block> name = new VertexNameProvider<Block>() {
//
//			@Override
//			public String getVertexName(Block b) {
//				String body = b.toString().replace("\'", "").replace("\"", "");
//				return body;
//			}
//		};
//
//		new File("sootOutput").mkdir();
//
//		DOTExporter<Block, DefaultEdge> exporter = new DOTExporter<Block, DefaultEdge>(id, name, null);
//		try {
//			exporter.export(new PrintWriter("sootOutput/" + testcase + ".dot"), graphP);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//
//		return;
//	}
	
	
}


//SCRAP AREA


//LAST PART OF internalBodyTransform. Add if needed.
//System.out.println("ExceptionGraph:" + graph);
//System.out.println(dg); 

//System.out.println("TOTALPATHS"+totalPaths);

//System.out.println("GC"+globalCounter);
//System.out.println("Instrumented blocks" + graph);

//Code for method signature
//String sd = b.getMethod().getSignature();
//System.out.println("METHOD BODY"+sd);

//SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>  dg = new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
//DirectedMultigraph<String, DefaultWeightedEdge>  dg = new DirectedMultigraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
//DirectedWeightedPseudograph<String, DefaultWeightedEdge>  dg = new DirectedWeightedPseudograph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);

