package pop_replay;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PoP_Replay_Util 
{     
	private static Deque<String> globalTrace1 = new ArrayDeque<String>();
	private static Deque<String> globalTrace = new ArrayDeque<String>(); // Global Trace without begin and end
	private static String[] threadIds =new String[10000]; //TID to String mapping
	private static int[] childsForked =new int[10000]; // TID to no. of children (used to increment ThreadId)	
	private static Integer myLock = new Integer(0);
	private static boolean turn = true;
	
    static {    	
            /* The global_trace will be copied to the current directory before running the test case */
            FileReader gt;
            try{
            	gt = new FileReader("global_trace");
    			Scanner gtScanner = new Scanner(gt);
    			while(gtScanner.hasNextLine())
    			{
    					globalTrace1.addLast(gtScanner.nextLine());
    			}
            	
    		} catch (FileNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            System.out.println("GT IS "+ globalTrace1);
 
            //Remove Begin and End traces
            String temp = globalTrace1.pop();
            while(!globalTrace1.isEmpty())
            {
            	if(temp.split(",")[1].trim().contentEquals("Begin") || temp.split(",")[1].trim().contentEquals("End"))
            		temp = globalTrace1.pop();
            	else
            	{
            		globalTrace.addLast(temp);
            		temp = globalTrace1.pop();
            	}                
            }
            System.out.println("GT IS "+ globalTrace);            
    }
	
	//Initialize at the beginning of main
	public static synchronized void addMainThread(){
		
		long pid = Thread.currentThread().getId();
		threadIds[(int)pid]="0";
	}
	
	//After tx.start()
	public static synchronized void addChildThread(long cid){
		long pid = Thread.currentThread().getId(); //get parent id
		threadIds[(int)cid]=threadIds[(int)pid]+"."+childsForked[(int)pid]; //cid=pid.threadCount
		childsForked[(int)pid]+=1;
	}
    
    /* You can modify criticalBefore() to receive the required arguments */
    public static void criticalBefore () {
   	
    	synchronized (myLock) {
    		long pid = Thread.currentThread().getId();
        	String TID = threadIds[(int)pid];
        	System.out.println("BEFORE TID IS"+TID);

        	while(true){
        		if (globalTrace.getFirst().split(",")[0].trim().contentEquals(TID.trim()) && turn == true) {
        			turn = false;					
					break;
				}
				try {
					myLock.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}        		
        	}	
		}
    }
    
    /* You can modify criticalAfter() to receive the required arguments */
    public static void criticalAfter () {
    	synchronized(myLock)
    	{
//    		long pid = Thread.currentThread().getId();
//        	String TID = threadIds[(int)pid];
//        	System.out.println("AFTER TID IS"+TID);
        	globalTrace.removeFirst();
//        	System.out.println();
        	turn = true;
        	myLock.notifyAll();
    	}

    }
       
}
