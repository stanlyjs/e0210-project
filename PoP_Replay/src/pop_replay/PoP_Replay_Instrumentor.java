package pop_replay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import soot.Body;
import soot.Local;
import soot.LongType;
import soot.PatchingChain;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IntConstant;
import soot.jimple.InterfaceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.LongConstant;
import soot.jimple.NullConstant;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class PoP_Replay_Instrumentor extends SceneTransformer
{    
	static SootClass popReplayUtil;
	static SootMethod criticalBefore, criticalAfter, addMainThread, addChildThread;
	static int init = 0;
	
    @Override
    protected void internalTransform(String arg0, Map<String, String> arg1) {
    	System.out.println("-----------------------------POP REPLAY UTIL--------------------------------");
        Chain<SootClass> allClasses = Scene.v().getApplicationClasses();
        
        if(init ==0) //Using static, jdk can initialize it during class loading or program loading; hence there was an error (class not found)	
        {
        	popReplayUtil = Scene.v().getSootClass(PoP_Replay_Util.class.getName());
    	    criticalBefore = popReplayUtil.getMethodByName("criticalBefore");
    	    criticalAfter = popReplayUtil.getMethodByName("criticalAfter");
    	    addMainThread = popReplayUtil.getMethodByName("addMainThread");
    	    addChildThread = popReplayUtil.getMethodByName("addChildThread");
    	    init =1;
        }
        
        for (SootClass curClass: allClasses) {
        	
            /* These classes must be skipped */
            if (curClass.getName().contains("pop_replay.PoP_Replay_Util")
             || curClass.getName().contains("popUtil.PoP_Util")
             || curClass.getName().contains("jdk.internal")) {
                continue;
            }
            
            List<SootMethod> allMethods = curClass.getMethods();
            
            for (SootMethod curMethod: allMethods) {   
            	if(curMethod.hasActiveBody())
            	{
            		Body b = curMethod.getActiveBody();
                    Local temp = Jimple.v().newLocal("tmpLocal", LongType.v()); 
                    b.getLocals().add(temp);
            		analyzeThreads(b, temp);
            		
            		boolean isFirstNonIdentityStmt = true;
            		
            		PatchingChain<Unit> units = b.getUnits();
            		Iterator<Unit> stmtIt = units.snapshotIterator(); //Snapshotiterator can handle concurrent modifications
            		while(stmtIt.hasNext())
            		{
            			Stmt s = (Stmt)stmtIt.next();
            			
            			if (!(s instanceof IdentityStmt) && isFirstNonIdentityStmt)
                        {
                        	if(b.getMethod().getSignature().contains("void <clinit>()"))
                        	{
                        		b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(addMainThread.makeRef())), s);
                            }
                        	isFirstNonIdentityStmt = false;
                        } 
            			
        				if(s instanceof AssignStmt)
        				{
        					
    						//ITT: Shared Variable Read ($r1=static_int_a)
    						if(((AssignStmt) s).getRightOp() instanceof StaticFieldRef && ((StaticFieldRef)((AssignStmt) s).getRightOp()).getType().toString().contains("java.lang"))
    						{
    							System.out.println("SHARED VARIABLE READ");
    							b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef())), s);
    							b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef())), s);
    						}
        					//ITT: Shared Variable Write
        					if(((AssignStmt) s).getLeftOp() instanceof StaticFieldRef && ((StaticFieldRef)((AssignStmt) s).getLeftOp()).getField().toString().contains("java.lang")) 
        					{
        						System.out.println("SHARED VARIABLE WRITE");
    							b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef())), s);
    							b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef())), s);
        					}
        				}
            			
            			if(s instanceof InvokeStmt)
        				{
        					InvokeExpr iexpr =((InvokeStmt) s).getInvokeExpr();
        					
        					if (iexpr instanceof VirtualInvokeExpr)
        					{
        						//ITT: For Fork
        						if(((VirtualInvokeExpr) iexpr).getMethod().getSignature().equals("<java.lang.Thread: void start()>"))
        						{
        							System.out.println("FORK");
        							b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef())), s);
        							b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef())), s);
        						}
        						
        						//ITT: For Join
        						if(((VirtualInvokeExpr) iexpr).getMethod().getSignature().equals("<java.lang.Thread: void join()>"))
        						{							
        							System.out.println("JOIN");
        							b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef())), s);
        							b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef())), s);
        						}
        						
        					}
        					
        					if(iexpr instanceof InterfaceInvokeExpr)
        					{
        						//ITT: For Lock
        						if(((InterfaceInvokeExpr) iexpr).getMethod().getSignature().equals("<java.util.concurrent.locks.Lock: void lock()>"))
        						{
        							System.out.println("LOCK");
        							b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef())), s);
        							b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef())), s);
        						}
        						
        						//ITT: For Unlock
        						if(((InterfaceInvokeExpr) iexpr).getMethod().getSignature().equals("<java.util.concurrent.locks.Lock: void unlock()>"))
        						{
        							System.out.println("UNLOCK");
        							b.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalBefore.makeRef())), s);
        							b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(criticalAfter.makeRef())), s);
        						}
        					}
        				}            			
            		}
            		System.out.println("BODY"+b);
            	}            	
            }  
        }  
    }
    
	//Add instrumentation to increment counter of child thread by 1
	void analyzeThreads(Body b, Local temp)
	{
		Iterator<Unit> stmtIt = b.getUnits().snapshotIterator();
		while(stmtIt.hasNext())
        {
            Stmt s = (Stmt) stmtIt.next();
            if (s instanceof InvokeStmt)
            {
                InvokeExpr iexpr = (InvokeExpr) ((InvokeStmt)s).getInvokeExpr();
                if (iexpr instanceof VirtualInvokeExpr)
                {       
                    if (((VirtualInvokeExpr)iexpr).getMethod().getSignature().equals("<java.lang.Thread: void start()>"))
                    {  
                    	System.out.println("BASE"+((VirtualInvokeExpr)iexpr).getBase());            	
                    	
                    	b.getUnits().insertAfter(Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(addChildThread.makeRef(),temp)), s);
                        
                    	//int a=$r0.getId()
                    	b.getUnits().insertAfter(Jimple.v().newAssignStmt(temp, Jimple.v().newVirtualInvokeExpr((Local) ((VirtualInvokeExpr)iexpr).getBase(), Scene.v().getMethod("<java.lang.Thread: long getId()>").makeRef())), s);
                    	}
                }
            }
            
        }
	}
}
